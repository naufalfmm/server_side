'use strict'

var PathController = require('../app/controllers/pathController');
var Road = require('../app/models/road'),
    Segment = require('../app/models/segment'),
    Path = require('../app/models/path'),
    Lane = require('../app/models/lane');
var db = require('../app/services/database');
var expect = require('chai').expect;

describe('Test Path Controller', () => {
    var roadId = null
    before(async () => {
        var newRoad = {
            road_name: "Road Name",
            road_population_range: 0
        }

        var road = await Road.create(newRoad)
        roadId = road.road_id
    })

    after(() => {
        Road.destroy({ where: { road_id: roadId } })
    })

    describe('Add Path', () => {
        describe('Succeed Add', () => {
            var segmentId = null
            var pathsId = []
            before(async () => {
                var newSegment = {
                    segment_start: 'Segment Start',
                    segment_end: 'Segment End',
                    segment_paths: 2,
                    segment_density: 0,
                    segment_road_id: roadId
                }

                var segment = await Segment.create(newSegment)
                segmentId = segment.segment_id
            })

            afterEach(() => {
                console.log("afterEach", pathsId)
                Path.destroy({ where: { path_id: pathsId } })
            })

            after(() => {
                Segment.destroy({ where: { segment_id: segmentId } })
            })

            it('Two Paths', (done) => {
                var newPaths = [
                    {
                        th: 0,
                        lanes: 2,
                        segmentId: segmentId
                    },
                    {
                        th: 1,
                        lanes: 2,
                        segmentId: segmentId
                    }
                ]

                PathController.addPathsOff(newPaths, null).then((res) => {
                    expect(res).to.have.property('status').to.equal(true)
                    expect(res).to.have.property('pathsId').with.lengthOf(2)
                    pathsId = res.pathsId
                    done()
                }).catch((err) => {
                    done(err)
                })
            })

            it('One Paths', (done) => {
                var newPath = [
                    {
                        th: 0,
                        lanes: 2,
                        segmentId: segmentId
                    }
                ]

                PathController.addPathsOff(newPath, null).then((res) => {
                    console.log("test",res.pathsId, pathsId)
                    expect(res).to.have.property('status').to.equal(true)
                    expect(res).to.have.property('pathsId')
                    pathsId.push(res.pathsId)
                    done()
                }).catch((err) => {
                    done(err)
                })
            })
        })

        describe('Failed Add', () => {
            var segmentId = null
            before(async () => {
                console.log("before Failed Add")
                var newSegment = {
                    segment_start: 'Segment Start',
                    segment_end: 'Segment End',
                    segment_paths: 2,
                    segment_density: 0,
                    segment_road_id: roadId
                }

                var segment = await Segment.create(newSegment)
                segmentId = segment.segment_id
            })

            after(() => {
                Segment.destroy({ where: { segment_id: segmentId } })
            })

            it('Undefined', (done) => {
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                var pathData = [
                    {
                        th: 0,
                        lanes: 2,
                        segmentId: segmentId
                    }
                ]

                var prop = ['th', 'lanes', 'segmentId']
                var propIdx = Math.floor(Math.random() * 3)

                delete pathData[0][prop[propIdx]]

                PathController.addPathsOff(pathData, null).then((res) => {
                    // console.log("test", res.pathsId, pathsId)
                    expect(res).to.have.property('code').to.equal(400)
                    expect(res).to.have.property('status').to.equal(false)
                    done()
                }).catch((err) => {
                    done(err)
                })
            })

            it('Empty', (done) => {
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                var pathData = [
                    {
                        th: 0,
                        lanes: 2,
                        segmentId: segmentId
                    }
                ]

                var prop = ['th', 'lanes', 'segmentId']
                var propIdx = Math.floor(Math.random() * 3)

                pathData[0][prop[propIdx]] = ''

                PathController.addPathsOff(pathData, null).then((res) => {
                    // console.log("test", res.pathsId, pathsId)
                    expect(res).to.have.property('code').to.equal(400)
                    expect(res).to.have.property('status').to.equal(false)
                    done()
                }).catch((err) => {
                    done(err)
                })
            })

            it('Path_th is not [0, 1]', (done) => {
                var newPath = [
                    {
                        th: 2,
                        lanes: 2,
                        segmentId: segmentId
                    }
                ]

                PathController.addPathsOff(newPath, null).then((res) => {
                    // console.log("Test",res)
                    expect(res).to.have.property('code').to.equal(403)
                    expect(res).to.have.property('status').to.equal(false)
                    done()
                }).catch((err) => {
                    done(err)
                })
            })

            describe('Number of Paths is more than Segment Path Value', () => {
                before(async () => {
                    console.log("before")
                    var newPaths = [
                        {
                            path_th: 0,
                            path_lanes: 2,
                            path_segment_id: segmentId
                        },
                        {
                            path_th: 1,
                            path_lanes: 2,
                            path_segment_id: segmentId
                        }
                    ]

                    await Path.bulkCreate(newPaths)
                })

                after(() => {
                    Path.destroy({ where: { path_segment_id: segmentId } })
                })

                it('Test', (done) => {
                    var newPath = [
                        {
                            th: 1,
                            lanes: 2,
                            segmentId: segmentId
                        }
                    ]

                    PathController.addPathsOff(newPath, null).then((res) => {
                        // console.log("Test",res)
                        expect(res).to.have.property('code').to.equal(403)
                        expect(res).to.have.property('status').to.equal(false)
                        done()
                    }).catch((err) => {
                        done(err)
                    })
                })
            })
        })
    })

    describe('Delete Path', () => {
        var segmentId = null
        before(async () => {
            var newSegment = {
                segment_start: 'Segment Start',
                segment_end: 'Segment End',
                segment_paths: 2,
                segment_density: 0,
                segment_road_id: roadId
            }

            var segment = await Segment.create(newSegment)
            segmentId = segment.segment_id
        })

        after(() => {
            Segment.destroy({ where: { segment_id: segmentId } })
        })

        describe('Succeed Delete', () => {
            var pathsId = [0,0]
            
            beforeEach(async () => {
                var newPath = {
                    path_th: 0,
                    path_lanes: 2,
                    path_segment_id: segmentId
                }

                var path = await Path.create(newPath)
                pathsId[0] = path.path_id

                newPath = {
                    path_th: 1,
                    path_lanes: 2,
                    path_segment_id: segmentId
                }

                path = await Path.create(newPath)
                pathsId[1] = path.path_id
            })

            it('Delete', (done) => {
                PathController.deletePathsOff(pathsId, null).then((res) => {
                    expect(res).to.have.property('status').to.equal(true)
                    done()
                }).catch((err) => {
                    done(err)
                })
            })
        })

        describe('Failed Delete', () => {
            var pathsId = [0, 0]

            beforeEach(async () => {
                var newPath = {
                    path_th: 0,
                    path_lanes: 2,
                    path_segment_id: segmentId
                }

                var path = await Path.create(newPath)
                pathsId[0] = path.path_id

                newPath = {
                    path_th: 1,
                    path_lanes: 2,
                    path_segment_id: segmentId
                }

                path = await Path.create(newPath)
                pathsId[1] = path.path_id
            })

            afterEach(() => {
                Path.destroy({ where: { path_id: pathsId } })
            })

            it('No Path Id', (done) => {
                var pathIds = [pathsId[0], 0]
                PathController.deletePathsOff(pathIds, null).then((res) => {
                    expect(res).to.have.property('code').to.equal(403)
                    expect(res).to.have.property('status').to.equal(false)
                    done()
                }).catch((err) => {
                    done(err)
                })
            })
        })
    })

    describe('Edit Path', () => {
        var trans, segmentId = null
        before(async () => {
            var newSegment = {
                segment_start: 'Segment Start',
                segment_end: 'Segment End',
                segment_paths: 2,
                segment_density: 0,
                segment_road_id: roadId
            }

            var segment = await Segment.create(newSegment)
            segmentId = segment.segment_id

            // trans = await db.transaction()
        })

        after(() => {
            Segment.destroy({ where: { segment_id: segmentId } })
        })

        describe('All Data', () => {
            var pathId = null
            before(async () => {
                var newPath = {
                    path_th: 0,
                    path_lanes: 3,
                    path_segment_id: segmentId
                }

                var path = await Path.create(newPath)
                pathId = path.path_id

                var newLanes = [
                    {
                        lane_th: 0,
                        lane_width: 3.15,
                        lane_cap: 123,
                        lane_path_id: pathId
                    },
                    {
                        lane_th: 1,
                        lane_width: 3.15,
                        lane_cap: 123,
                        lane_path_id: pathId
                    },
                    {
                        lane_th: 2,
                        lane_width: 3.15,
                        lane_cap: 123,
                        lane_path_id: pathId
                    }
                ]

                await Lane.bulkCreate(newLanes)
            })

            after(() => {
                Path.destroy({ where: { path_id: pathId } })
            })

            it('Edited path_lanes number is less than path_lanes number', (done) => {
                var lanes = Math.floor(Math.random() * 3) + 1
                console.log("test",lanes)
                var editedPath = [
                    {  
                        id: pathId,
                        th: 0,
                        lanes: lanes
                    }
                ]

                PathController.editPathsOff(editedPath, null).then((res) => {
                    console.log(res)
                    expect(res).to.have.property('status').to.equal(true)
                    Lane.findAll({ where: { lane_path_id: pathId } }).then((res) => {
                        expect(res).to.have.lengthOf(lanes)
                        done()
                    }).catch((err) => {
                        done(err)
                    })
                }).catch((err) => {
                    done(err)
                })
            })

            describe('Failed Edit', () => {
                it('Undefined', (done) => {
                    var editedPath = [
                        {
                            id: pathId,
                            th: 0,
                            lanes: 2
                        }
                    ]

                    var prop = ['id', 'th', 'lanes']
                    var propIdx = Math.floor(Math.random() * 3)

                    delete editedPath[0][prop[propIdx]]

                    PathController.editPathsOff(editedPath, null).then((res) => {
                        expect(res).to.have.property('code').to.equal(400)
                        expect(res).to.have.property('status').to.equal(false)
                        done()
                    }).catch((err) => {
                        done(err)
                    })
                })

                it('Empty', (done) => {
                    var editedPath = [
                        {
                            id: pathId,
                            th: 0,
                            lanes: 2
                        }
                    ]

                    var prop = ['id', 'th', 'lanes']
                    var propIdx = Math.floor(Math.random() * 3)

                    editedPath[0][prop[propIdx]] = ''

                    PathController.editPathsOff(editedPath, null).then((res) => {
                        expect(res).to.have.property('code').to.equal(400)
                        expect(res).to.have.property('status').to.equal(false)
                        done()
                    }).catch((err) => {
                        done(err)
                    })
                })
            })
        })

        describe('Changed Th', () => {
            describe('Exchange Th', () => {
                var pathsId = [0,0]
                before(async () => {
                    var newPath = {
                        path_th: 0,
                        path_lanes: 3,
                        path_segment_id: segmentId
                    }

                    var path = await Path.create(newPath)
                    pathsId[0] = path.path_id

                    newPath = {
                        path_th: 1,
                        path_lanes: 3,
                        path_segment_id: segmentId
                    }

                    path = await Path.create(newPath)
                    pathsId[1] = path.path_id
                })

                after(() => {
                    Path.destroy({ where: { path_id: pathsId } })
                })

                it('Successfully Exchanged Th', (done) => {
                    var editedPath = [
                        {  
                            id: pathsId[0],
                            th: 1,
                            lanes: 3
                        },
                        {
                            id: pathsId[1],
                            th: 0,
                            lanes: 3
                        }
                    ]

                    PathController.editPathsOff(editedPath,null).then((res) => {
                        expect(res).to.have.property('status').to.equal(true)
                        done()
                    }).catch((err) => {
                        done(err)
                    })
                })
            })

            describe('0 -> 1', () => {
                describe('Th 1 Not Exists', () => {
                    var pathId = null
                    before(async () => {
                        // console.log("beforeHook", segmentId)
                        var newPath = {
                            path_th: 0,
                            path_lanes: 3,
                            path_segment_id: segmentId
                        }

                        var path = await Path.create(newPath)
                        pathId = path.path_id
                    })
                    
                    after(() => {
                        // console.log("after")
                        Path.destroy({ where: { path_id: pathId } })
                    })

                    it('Test', (done) => {
                        // console.log('Test')
                        var editedPath = [
                            {
                                id: pathId,
                                th: 1,
                                lanes: 3
                            }
                        ]

                        PathController.editPathsOff(editedPath, null).then((res) => {
                            console.log(res)
                            expect(res).to.have.property('code').to.equal(403)
                            expect(res).to.have.property('status').to.equal(false)
                            done()
                        }).catch((err) => {
                            done(err)
                        })
                    })
                })

                describe('Duplicated Th 1', () => {
                    var pathsId = [0,0]
                    before(async () => {
                        // console.log("beforeHook", segmentId)
                        var newPath = {
                            path_th: 0,
                            path_lanes: 3,
                            path_segment_id: segmentId
                        }

                        var path = await Path.create(newPath)
                        pathsId[0] = path.path_id

                        newPath = {
                            path_th: 1,
                            path_lanes: 3,
                            path_segment_id: segmentId
                        }

                        path = await Path.create(newPath)
                        pathsId[1] = path.path_id
                    })

                    after(() => {
                        // console.log("after")
                        Path.destroy({ where: { path_id: pathsId } })
                    })

                    it('Test', (done) => {
                        // console.log('Test')
                        var editedPath = [
                            {
                                id: pathsId[0],
                                th: 1,
                                lanes: 3
                            }
                        ]

                        PathController.editPathsOff(editedPath, null).then((res) => {
                            console.log(res)
                            expect(res).to.have.property('code').to.equal(403)
                            expect(res).to.have.property('status').to.equal(false)
                            done()
                        }).catch((err) => {
                            done(err)
                        })
                    })
                })
            })

            describe('1 -> 0', () => {
                describe('Duplicated Th 0', () => {
                    var pathsId = [0, 0]
                    before(async () => {
                        // console.log("beforeHook", segmentId)
                        var newPath = {
                            path_th: 0,
                            path_lanes: 3,
                            path_segment_id: segmentId
                        }

                        var path = await Path.create(newPath)
                        pathsId[0] = path.path_id

                        newPath = {
                            path_th: 1,
                            path_lanes: 3,
                            path_segment_id: segmentId
                        }

                        path = await Path.create(newPath)
                        pathsId[1] = path.path_id
                    })

                    after(() => {
                        // console.log("after")
                        Path.destroy({ where: { path_id: pathsId } })
                    })

                    it('Test', (done) => {
                        // console.log('Test')
                        var editedPath = [
                            {
                                id: pathsId[1],
                                th: 0,
                                lanes: 3
                            }
                        ]

                        PathController.editPathsOff(editedPath, null).then((res) => {
                            console.log(res)
                            expect(res).to.have.property('code').to.equal(403)
                            expect(res).to.have.property('status').to.equal(false)
                            done()
                        }).catch((err) => {
                            done(err)
                        })
                    })
                })
            })
        })
    })
})