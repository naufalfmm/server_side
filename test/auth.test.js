'use strict'

var AuthController = require('../app/controllers/authController');
var Admin = require('../app/models/admin'),
    Node = require('../app/models/node');
var expect = require('chai').expect
// var assert = require('assert');

process.env.NODE_ENV = 'test'

// chai.config.includeStack = true;

describe('Testing Auth Controller', () => {
    describe('Check JWT', () => {
        it('JWT Correct', (done) => {
            var token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI"
            AuthController.checkAuthentication(token, 0).then((resl) => {
                // console.log(resl)
                expect(resl).to.have.property('code').to.equal(202)
                expect(resl).to.have.property('status').to.equal(true)
                expect(resl).to.have.property('message').to.equal("JWT Valid")
                done()
            }).catch(err => {
                done(err)
            })
        })

        it('JWT Expired', (done) => {
            var token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6OTE1MTIzNTk5LCJleHAiOjkxNTEyMzU5OX0.fSTjM1JbjB0c-vzF2z1oext90ms49S1j5LXB3SMAqZI"
            AuthController.checkAuthentication(token, 0).then((resl) => {
                // console.log(resl)
                expect(resl).to.have.property('code').to.equal(401)
                expect(resl).to.have.property('status').to.equal(false)
                expect(resl).to.have.property('message').to.equal("jwt expired")
                done()
            }).catch(err => {
                done(err)
            })
        })
    })
    describe('Super Admin and Admin Role Authorization', () => {
        before(async () => {
            // console.log("Before")
            var newAdmin = {
                admin_username: "admin",
                admin_password: "admin",
                admin_name: "Admin",
                admin_email: "admin@cds.com",
                admin_grade: 1,
                admin_active: 0
            }

            await Admin.create(newAdmin)
        })

        after(() => {
            // console.log("After")
            Admin.destroy({where: {admin_username: "admin"}})
        })

        it('Admin role is should unauthorized for Super Admin role access', (done) => {
            var token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwic3RhdHVzIjoiYWRtaW4iLCJpYXQiOjcyODk2MjkxOTksImV4cCI6NzI4OTYyOTE5OX0.ISDJn1dgzEagqQWPlnvrLSHzu4EPoFjN7gQ-vpJWsWE"
            AuthController.checkAuthentication(token, 0).then((resl) => {
                expect(resl).to.have.property('code').to.equal(401)
                expect(resl).to.have.property('status').to.equal(false)
                expect(resl).to.have.property('message').to.equal("Unauthorized")
                done()
            }).catch(err => {
                done(err)
            })
        })

        it('Super Admin role is should authorized for Admin role access', (done) => {
            var token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI"
            AuthController.checkAuthentication(token, 1).then((resl) => {
                expect(resl).to.have.property('code').to.equal(202)
                expect(resl).to.have.property('status').to.equal(true)
                done()
            }).catch(err => {
                done(err)
            })
        })
    })
    describe('Super Admin, Admin, and Node Role Authorization', () => {
        it('Admin role is should unauthorized for Node role access', (done) => {
            var token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwic3RhdHVzIjoiYWRtaW4iLCJpYXQiOjcyODk2MjkxOTksImV4cCI6NzI4OTYyOTE5OX0.ISDJn1dgzEagqQWPlnvrLSHzu4EPoFjN7gQ-vpJWsWE"
            AuthController.checkAuthentication(token, 2).then((resl) => {
                expect(resl).to.have.property('code').to.equal(401)
                expect(resl).to.have.property('status').to.equal(false)
                done()
            }).catch(err => {
                done(err)
            })
        })

        it('Super Admin role is should unauthorized for Node role access', (done) => {
            var token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI"
            AuthController.checkAuthentication(token, 2).then((resl) => {
                expect(resl).to.have.property('code').to.equal(401)
                expect(resl).to.have.property('status').to.equal(false)
                done()
            }).catch(err => {
                done(err)
            })
        })
    })
})

