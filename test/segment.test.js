'use strict'

var request = require('supertest'),
    expect = require('chai').expect;
var app = require('../app');
var Road = require('../app/models/road'),
    Segment = require('../app/models/segment'),
    Path = require('../app/models/path'),
    Lane = require('../app/models/lane');
var Sequelize = require('sequelize');

describe('Testing Segment Controller', () => {
    var roadId = null
    before(async () => {
        var newRoad = {
            road_name: "Road Name",
            road_population_range: 0
        }

        var road = await Road.create(newRoad)
        roadId = road.road_id
    })

    after(() => {
        Road.destroy({ where: { road_id: roadId } })
    })

    describe('POST Add Segment', () => {
        describe('Succeed Add', () => {
            afterEach(() => {
                console.log("afterEach")
                Segment.destroy({ where: { segment_road_id: roadId } })
            })

            it('Full Data', (done) => {
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                var segmentData = {
                    start: 'Segment Start',
                    end: 'Segment End',
                    paths: 2,
                    density: 0,
                    firstPath: {
                        th: 0,
                        lanes: 2
                    },
                    secondPath: {
                        th: 1,
                        lanes: 2
                    }
                }

                request(app).post('/api/segment/add/' + roadId).set(header).send(segmentData).end((err, res) => {
                    if (err) done(err)
                    expect(res.statusCode).to.equal(201)
                    expect(res.body).to.have.property('status').to.equal(true)
                    Segment.findOne({ where: { segment_road_id: roadId }, include: [{ model: Path }] }).then(res => {
                        expect(res.paths).to.be.an('array').with.lengthOf(2)
                        done()
                    }).catch(err => {
                        done(err)
                    })
                })
            })

            it('No Second Path (Path of Segment Number is 1)', (done) => {
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                var segmentData = {
                    start: 'Segment Start',
                    end: 'Segment End',
                    paths: 1,
                    density: 0,
                    firstPath: {
                        th: 0,
                        lanes: 2
                    }
                }

                request(app).post('/api/segment/add/' + roadId).set(header).send(segmentData).end((err, res) => {
                    if (err) done(err)
                    expect(res.statusCode).to.equal(201)
                    expect(res.body).to.have.property('status').to.equal(true)
                    Segment.findOne({ where: { segment_road_id: roadId }, include: [{ model: Path }] }).then(res => {
                        expect(res.paths).to.be.an('array').with.lengthOf(1)
                        done()
                    }).catch(err => {
                        done(err)
                    })
                })
            })
        })

        describe('Failed Add', () => {
            it('Undefined', (done) => {
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                var segmentData = {
                    start: 'Segment Start',
                    end: 'Segment End',
                    paths: 1,
                    density: 0,
                    firstPath: {
                        th: 0,
                        lanes: 2
                    }
                }

                var prop = ['start', 'end', 'paths', 'density', 'firstPath']
                var propIdx = Math.floor(Math.random() * 5)

                delete segmentData[prop[propIdx]]

                request(app).post('/api/segment/add/' + roadId).set(header).send(segmentData).end((err, res) => {
                    if (err) done(err)
                    expect(res.statusCode).to.equal(400)
                    expect(res.body).to.have.property('status').to.equal(false)
                    done()
                })
            })

            it('Empty', (done) => {
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                var segmentData = {
                    start: 'Segment Start',
                    end: 'Segment End',
                    paths: 1,
                    density: 0,
                    firstPath: {
                        th: 0,
                        lanes: 2
                    }
                }

                var prop = ['start', 'end', 'paths', 'density']
                var propIdx = Math.floor(Math.random() * 4)

                segmentData[prop[propIdx]] = ''

                request(app).post('/api/segment/add/' + roadId).set(header).send(segmentData).end((err, res) => {
                    if (err) done(err)
                    expect(res.statusCode).to.equal(400)
                    expect(res.body).to.have.property('status').to.equal(false)
                    done()
                })
            })

            it('Number of Segment Path is 2 but no second path', (done) => {
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                var segmentData = {
                    start: 'Segment Start',
                    end: 'Segment End',
                    paths: 2,
                    density: 0,
                    firstPath: {
                        th: 0,
                        lanes: 2
                    }
                }

                request(app).post('/api/segment/add/' + roadId).set(header).send(segmentData).end((err, res) => {
                    if (err) done(err)
                    expect(res.statusCode).to.equal(400)
                    expect(res.body).to.have.property('status').to.equal(false)
                    done()
                })
            })

            it('Number of Segment Path is more than 2', (done) => {
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                var segmentData = {
                    start: 'Segment Start',
                    end: 'Segment End',
                    paths: 3,
                    density: 0,
                    firstPath: {
                        th: 0,
                        lanes: 2
                    },
                    secondPath: {
                        th: 1,
                        lanes: 2
                    }
                }

                request(app).post('/api/segment/add/' + roadId).set(header).send(segmentData).end((err, res) => {
                    if (err) done(err)
                    expect(res.statusCode).to.equal(403)
                    expect(res.body).to.have.property('status').to.equal(false)
                    done()
                })
            })
        })
    })

    describe('DELETE Delete Segment', () => {
        describe('Succeed Delete', () => {
            var segmentId = null
            before(async () => {
                var newSegment = {
                    segment_start: 'Segment Start',
                    segment_end: 'Segment End',
                    segment_paths: 1,
                    segment_density: 0,
                    segment_road_id: roadId
                }

                var segment = await Segment.create(newSegment)
                segmentId = segment.segment_id
            })

            it('Delete', (done) => {
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                request(app).delete('/api/segment/delete/' + segmentId).set(header).end((err, res) => {
                    if (err) done(err)
                    expect(res.statusCode).to.equal(200)
                    expect(res.body).to.have.property('status').to.equal(true)
                    done()
                })
            })
        })

        describe('Failed Delete', () => {
            it('No Segment Id Exist', (done) => {
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                request(app).delete('/api/segment/delete/' + 0).set(header).end((err, res) => {
                    if (err) done(err)
                    expect(res.statusCode).to.equal(403)
                    expect(res.body).to.have.property('status').to.equal(false)
                    done()
                })
            })
        })
    })

    describe('POST Edit Segment', () => {
        describe('All Data Except Path', () => {
            var segmentId, pathId = null
            before(async () => {
                var newSegment = {
                    segment_start: 'Segment Start',
                    segment_end: 'Segment End',
                    segment_paths: 1,
                    segment_density: 0,
                    segment_road_id: roadId
                }

                var segment = await Segment.create(newSegment)
                segmentId = segment.segment_id

                var newPath = {
                    path_th: 0,
                    path_lanes: 2,
                    path_segment_id: segmentId
                }

                var path = await Path.create(newPath)
                pathId = path.path_id
            })

            after(() => {
                Segment.destroy({ where: { segment_id: segmentId } })
            })

            it('Succeed Edit', (done) => {
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                var segmentData = {
                    start: 'Segment Start',
                    end: 'Segment End',
                    paths: 1,
                    density: 2,
                    firstPath: {
                        id: pathId,
                        th: 0,
                        lanes: 2
                    }
                }

                request(app).post('/api/segment/edit/' + segmentId).set(header).send(segmentData).end((err, res) => {
                    if (err) done(err)
                    expect(res.statusCode).to.equal(200)
                    expect(res.body).to.have.property('status').to.equal(true)
                    done()
                })
            })

            describe('Failed Edit', () => {
                it('Undefined', (done) => {
                    var header = {
                        'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                        'Content-Type': 'application/json'
                    }

                    var segmentData = {
                        start: 'Segment Start',
                        end: 'Segment End',
                        paths: 1,
                        density: 2,
                        firstPath: {
                            id: pathId,
                            th: 0,
                            lanes: 2
                        }
                    }

                    var prop = ['start', 'end', 'paths', 'density', 'firstPath']
                    var propIdx = Math.floor(Math.random() * 5)

                    delete segmentData[prop[propIdx]]

                    request(app).post('/api/segment/edit/' + segmentId).set(header).send(segmentData).end((err, res) => {
                        if (err) done(err)
                        expect(res.statusCode).to.equal(400)
                        expect(res.body).to.have.property('status').to.equal(false)
                        done()
                    })
                })

                it('Empty', (done) => {
                    var header = {
                        'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                        'Content-Type': 'application/json'
                    }

                    var segmentData = {
                        start: 'Segment Start',
                        end: 'Segment End',
                        paths: 1,
                        density: 2,
                        firstPath: {
                            id: pathId,
                            th: 0,
                            lanes: 2
                        }
                    }

                    var prop = ['start', 'end', 'paths', 'density']
                    var propIdx = Math.floor(Math.random() * 4)

                    segmentData[prop[propIdx]] = ''

                    request(app).post('/api/segment/edit/' + segmentId).set(header).send(segmentData).end((err, res) => {
                        if (err) done(err)
                        expect(res.statusCode).to.equal(400)
                        expect(res.body).to.have.property('status').to.equal(false)
                        done()
                    })
                })
            })
        })

        describe('2 -> 1', () => {
            var segmentId = null
            before(async () => {
                var newSegment = {
                    segment_start: 'Segment Start',
                    segment_end: 'Segment End',
                    segment_paths: 2,
                    segment_density: 0,
                    segment_road_id: roadId
                }

                var segment = await Segment.create(newSegment)
                segmentId = segment.segment_id
            })

            after(() => {
                Segment.destroy({ where: { segment_id: segmentId } })
            })

            describe('First Condition', () => {
                var pathsId = [0, 0]
                before(async () => {
                    var newPath = {
                        path_th: 0,
                        path_lanes: 2,
                        path_segment_id: segmentId
                    }

                    var path = await Path.create(newPath)
                    pathsId[0] = path.path_id

                    var newLanes = [
                        {
                            lane_th: 0,
                            lane_width: 3.15,
                            lane_cap: 123,
                            lane_path_id: pathsId[0]
                        },
                        {
                            lane_th: 1,
                            lane_width: 3.15,
                            lane_cap: 123,
                            lane_path_id: pathsId[0]
                        }
                    ]

                    await Lane.bulkCreate(newLanes)

                    newPath = {
                        path_th: 1,
                        path_lanes: 2,
                        path_segment_id: segmentId
                    }

                    path = await Path.create(newPath)
                    pathsId[1] = path.path_id
                })

                after(async () => {
                    console.log("after", pathsId)
                    await Path.destroy({ where: { path_id: pathsId[0] } })
                })
                
                it('2,2 -> 4', (done) => {
                    var header = {
                        'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                        'Content-Type': 'application/json'
                    }

                    var segmentData = {
                        start: 'Segment Start',
                        end: 'Segment End',
                        paths: 1,
                        density: 2,
                        firstPath: {
                            id: pathsId[0],
                            th: 0,
                            lanes: 4
                        }
                    }

                    request(app).post('/api/segment/edit/' + segmentId).set(header).send(segmentData).end((err, res) => {
                        if (err) done(err)
                        expect(res.statusCode).to.equal(200)
                        expect(res.body).to.have.property('status').to.equal(true)
                        Segment.findOne({ where: { segment_road_id: roadId }, include: [{ model: Path, include: [{ model: Lane }] }] }).then(res => {
                            expect(res.paths).to.be.an('array').with.lengthOf(1)
                            var paths = res.paths[0]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(2)
                            var ths = paths.lanes.map(l => l.lane_th)
                            expect(ths).to.have.members([0, 1])
                            done()
                        }).catch(err => {
                            done(err)
                        })
                    })
                })
            })

            describe('Second Condition', () => {
                var pathsId = [0, 0]
                before(async () => {
                    await Segment.update({ segment_paths: 2 }, { where: { segment_id: segmentId } })

                    var newPath = {
                        path_th: 0,
                        path_lanes: 2,
                        path_segment_id: segmentId
                    }

                    var path = await Path.create(newPath)
                    pathsId[0] = path.path_id

                    newPath = {
                        path_th: 1,
                        path_lanes: 2,
                        path_segment_id: segmentId
                    }

                    path = await Path.create(newPath)
                    pathsId[1] = path.path_id

                    var newLanes = [
                        {
                            lane_th: 0,
                            lane_width: 3.15,
                            lane_cap: 123,
                            lane_path_id: pathsId[0]
                        },
                        {
                            lane_th: 0,
                            lane_width: 3.15,
                            lane_cap: 123,
                            lane_path_id: pathsId[1]
                        }
                    ]

                    await Lane.bulkCreate(newLanes)
                })

                after(() => {
                    console.log("after", pathsId)
                    Path.destroy({ where: { path_id: pathsId[0] } })
                })

                it('2,2 -> 4', (done) => {
                    var header = {
                        'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                        'Content-Type': 'application/json'
                    }

                    var segmentData = {
                        start: 'Segment Start',
                        end: 'Segment End',
                        paths: 1,
                        density: 2,
                        firstPath: {
                            id: pathsId[0],
                            th: 0,
                            lanes: 4
                        }
                    }

                    request(app).post('/api/segment/edit/' + segmentId).set(header).send(segmentData).end((err, res) => {
                        if (err) done(err)
                        expect(res.statusCode).to.equal(200)
                        expect(res.body).to.have.property('status').to.equal(true)
                        Segment.findOne({ where: { segment_road_id: roadId }, include: [{ model: Path, include: [{ model: Lane }] }] }).then(res => {
                            // console.log(res.paths)
                            expect(res.paths).to.be.an('array').with.lengthOf(1)
                            var paths = res.paths[0]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(2)
                            var ths = paths.lanes.map(l => l.lane_th)
                            expect(ths).to.have.members([0, 2])
                            done()
                        }).catch(err => {
                            done(err)
                        })
                    })
                })
            })

            describe('Third Condition', () => {
                var pathsId = [0, 0]
                beforeEach(async () => {
                    await Segment.update({ segment_paths: 2 }, { where: { segment_id: segmentId } })

                    var newPath = {
                        path_th: 0,
                        path_lanes: 2,
                        path_segment_id: segmentId
                    }

                    var path = await Path.create(newPath)
                    pathsId[0] = path.path_id

                    newPath = {
                        path_th: 1,
                        path_lanes: 2,
                        path_segment_id: segmentId
                    }

                    path = await Path.create(newPath)
                    pathsId[1] = path.path_id

                    var newLanes = [
                        {
                            lane_th: 0,
                            lane_width: 3.15,
                            lane_cap: 123,
                            lane_path_id: pathsId[0]
                        },
                        {
                            lane_th: 1,
                            lane_width: 3.15,
                            lane_cap: 123,
                            lane_path_id: pathsId[1]
                        }
                    ]

                    await Lane.bulkCreate(newLanes)
                })

                afterEach(() => {
                    console.log("afterEach", pathsId)
                    Path.destroy({ where: { path_id: pathsId[0] } })
                })

                it('2,2 -> 4', (done) => {
                    var header = {
                        'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                        'Content-Type': 'application/json'
                    }

                    var segmentData = {
                        start: 'Segment Start',
                        end: 'Segment End',
                        paths: 1,
                        density: 2,
                        firstPath: {
                            id: pathsId[0],
                            th: 0,
                            lanes: 4
                        }
                    }

                    request(app).post('/api/segment/edit/' + segmentId).set(header).send(segmentData).end((err, res) => {
                        if (err) done(err)
                        expect(res.statusCode).to.equal(200)
                        expect(res.body).to.have.property('status').to.equal(true)
                        Segment.findOne({ where: { segment_road_id: roadId }, include: [{ model: Path, include: [{ model: Lane }] }] }).then(res => {
                            expect(res.paths).to.be.an('array').with.lengthOf(1)
                            var paths = res.paths[0]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(2)
                            var ths = paths.lanes.map(l => l.lane_th)
                            expect(ths).to.have.members([0, 3])
                            done()
                        }).catch(err => {
                            done(err)
                        })
                    })
                })

                it('2,2 -> 3', (done) => {
                    var header = {
                        'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                        'Content-Type': 'application/json'
                    }

                    var segmentData = {
                        start: 'Segment Start',
                        end: 'Segment End',
                        paths: 1,
                        density: 2,
                        firstPath: {
                            id: pathsId[0],
                            th: 0,
                            lanes: 3
                        }
                    }

                    request(app).post('/api/segment/edit/' + segmentId).set(header).send(segmentData).end((err, res) => {
                        if (err) done(err)
                        expect(res.statusCode).to.equal(200)
                        expect(res.body).to.have.property('status').to.equal(true)
                        Segment.findOne({ where: { segment_road_id: roadId }, include: [{ model: Path, include: [{ model: Lane }] }] }).then(res => {
                            expect(res.paths).to.be.an('array').with.lengthOf(1)
                            var paths = res.paths[0]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(1)
                            var ths = paths.lanes.map(l => l.lane_th)
                            expect(ths).to.have.members([0])
                            done()
                        }).catch(err => {
                            done(err)
                        })
                    })
                })                
            })

            describe('Fourth Condition', () => {
                var pathsId = [0, 0]
                before(async () => {
                    await Segment.update({ segment_paths: 2 }, { where: { segment_id: segmentId } })
                    
                    var newPath = {
                        path_th: 0,
                        path_lanes: 2,
                        path_segment_id: segmentId
                    }

                    var path = await Path.create(newPath)
                    pathsId[0] = path.path_id

                    newPath = {
                        path_th: 1,
                        path_lanes: 2,
                        path_segment_id: segmentId
                    }

                    path = await Path.create(newPath)
                    pathsId[1] = path.path_id

                    var newLanes = [
                        {
                            lane_th: 1,
                            lane_width: 3.15,
                            lane_cap: 123,
                            lane_path_id: pathsId[0]
                        },
                        {
                            lane_th: 0,
                            lane_width: 3.15,
                            lane_cap: 123,
                            lane_path_id: pathsId[1]
                        }
                    ]

                    await Lane.bulkCreate(newLanes)
                })

                after(() => {
                    console.log("after", pathsId)
                    Path.destroy({ where: { path_id: pathsId[0] } })
                })

                it('2,2 -> 4', (done) => {
                    var header = {
                        'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                        'Content-Type': 'application/json'
                    }

                    var segmentData = {
                        start: 'Segment Start',
                        end: 'Segment End',
                        paths: 1,
                        density: 2,
                        firstPath: {
                            id: pathsId[0],
                            th: 0,
                            lanes: 4
                        }
                    }

                    request(app).post('/api/segment/edit/' + segmentId).set(header).send(segmentData).end((err, res) => {
                        if (err) done(err)
                        expect(res.statusCode).to.equal(200)
                        expect(res.body).to.have.property('status').to.equal(true)
                        Segment.findOne({ where: { segment_road_id: roadId }, include: [{ model: Path, include: [{ model: Lane }] }] }).then(res => {
                            expect(res.paths).to.be.an('array').with.lengthOf(1)
                            var paths = res.paths[0]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(2)
                            var ths = paths.lanes.map(l => l.lane_th)
                            expect(ths).to.have.members([1, 2])
                            done()
                        }).catch(err => {
                            done(err)
                        })
                    })
                })
            })

            describe('Fifth Condition', () => {
                var pathsId = [0, 0]
                beforeEach(async () => {
                    await Segment.update({ segment_paths: 2 }, { where: { segment_id: segmentId } })

                    var newPath = {
                        path_th: 0,
                        path_lanes: 2,
                        path_segment_id: segmentId
                    }

                    var path = await Path.create(newPath)
                    pathsId[0] = path.path_id

                    newPath = {
                        path_th: 1,
                        path_lanes: 2,
                        path_segment_id: segmentId
                    }

                    path = await Path.create(newPath)
                    pathsId[1] = path.path_id

                    var newLanes = [
                        {
                            lane_th: 1,
                            lane_width: 3.15,
                            lane_cap: 123,
                            lane_path_id: pathsId[0]
                        },
                        {
                            lane_th: 1,
                            lane_width: 3.15,
                            lane_cap: 123,
                            lane_path_id: pathsId[1]
                        }
                    ]

                    await Lane.bulkCreate(newLanes)
                })

                afterEach(() => {
                    console.log("after", pathsId)
                    Path.destroy({ where: { path_id: pathsId[0] } })
                })

                it('2,2 -> 4', (done) => {
                    var header = {
                        'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                        'Content-Type': 'application/json'
                    }

                    var segmentData = {
                        start: 'Segment Start',
                        end: 'Segment End',
                        paths: 1,
                        density: 2,
                        firstPath: {
                            id: pathsId[0],
                            th: 0,
                            lanes: 4
                        }
                    }

                    request(app).post('/api/segment/edit/' + segmentId).set(header).send(segmentData).end((err, res) => {
                        if (err) done(err)
                        expect(res.statusCode).to.equal(200)
                        expect(res.body).to.have.property('status').to.equal(true)
                        Segment.findOne({ where: { segment_road_id: roadId }, include: [{ model: Path, include: [{ model: Lane }] }] }).then(res => {
                            expect(res.paths).to.be.an('array').with.lengthOf(1)
                            var paths = res.paths[0]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(2)
                            var ths = paths.lanes.map(l => l.lane_th)
                            expect(ths).to.have.members([1, 3])
                            done()
                        }).catch(err => {
                            done(err)
                        })
                    })
                })

                it('2,2 -> 3', (done) => {
                    var header = {
                        'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                        'Content-Type': 'application/json'
                    }

                    var segmentData = {
                        start: 'Segment Start',
                        end: 'Segment End',
                        paths: 1,
                        density: 2,
                        firstPath: {
                            id: pathsId[0],
                            th: 0,
                            lanes: 3
                        }
                    }

                    request(app).post('/api/segment/edit/' + segmentId).set(header).send(segmentData).end((err, res) => {
                        if (err) done(err)
                        expect(res.statusCode).to.equal(200)
                        expect(res.body).to.have.property('status').to.equal(true)
                        Segment.findOne({ where: { segment_road_id: roadId }, include: [{ model: Path, include: [{ model: Lane }] }] }).then(res => {
                            expect(res.paths).to.be.an('array').with.lengthOf(1)
                            var paths = res.paths[0]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(1)
                            var ths = paths.lanes.map(l => l.lane_th)
                            expect(ths).to.have.members([1])
                            done()
                        }).catch(err => {
                            done(err)
                        })
                    })
                })
            })

            describe('Sixth Condition', () => {
                var pathsId = [0, 0]
                before(async () => {
                    await Segment.update({ segment_paths: 2 }, { where: { segment_id: segmentId } })
                    
                    var newPath = {
                        path_th: 0,
                        path_lanes: 2,
                        path_segment_id: segmentId
                    }

                    var path = await Path.create(newPath)
                    pathsId[0] = path.path_id

                    newPath = {
                        path_th: 1,
                        path_lanes: 2,
                        path_segment_id: segmentId
                    }

                    path = await Path.create(newPath)
                    pathsId[1] = path.path_id

                    var newLanes = [
                        {
                            lane_th: 0,
                            lane_width: 3.15,
                            lane_cap: 123,
                            lane_path_id: pathsId[1]
                        },
                        {
                            lane_th: 1,
                            lane_width: 3.15,
                            lane_cap: 123,
                            lane_path_id: pathsId[1]
                        }
                    ]

                    await Lane.bulkCreate(newLanes)
                })

                after(() => {
                    console.log("after", pathsId)
                    Path.destroy({ where: { path_id: pathsId[0] } })
                })

                it('2,2 -> 4', (done) => {
                    var header = {
                        'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                        'Content-Type': 'application/json'
                    }

                    var segmentData = {
                        start: 'Segment Start',
                        end: 'Segment End',
                        paths: 1,
                        density: 2,
                        firstPath: {
                            id: pathsId[0],
                            th: 0,
                            lanes: 4
                        }
                    }

                    request(app).post('/api/segment/edit/' + segmentId).set(header).send(segmentData).end((err, res) => {
                        if (err) done(err)
                        expect(res.statusCode).to.equal(200)
                        expect(res.body).to.have.property('status').to.equal(true)
                        Segment.findOne({ where: { segment_road_id: roadId }, include: [{ model: Path, include: [{ model: Lane }] }] }).then(res => {
                            expect(res.paths).to.be.an('array').with.lengthOf(1)
                            var paths = res.paths[0]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(2)
                            var ths = paths.lanes.map(l => l.lane_th)
                            expect(ths).to.have.members([2, 3])
                            done()
                        }).catch(err => {
                            done(err)
                        })
                    })
                })

                it('2,2 -> 3', (done) => {
                    var header = {
                        'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                        'Content-Type': 'application/json'
                    }

                    var segmentData = {
                        start: 'Segment Start',
                        end: 'Segment End',
                        paths: 1,
                        density: 2,
                        firstPath: {
                            id: pathsId[0],
                            th: 0,
                            lanes: 3
                        }
                    }

                    request(app).post('/api/segment/edit/' + segmentId).set(header).send(segmentData).end((err, res) => {
                        if (err) done(err)
                        expect(res.statusCode).to.equal(200)
                        expect(res.body).to.have.property('status').to.equal(true)
                        Segment.findOne({ where: { segment_road_id: roadId }, include: [{ model: Path, include: [{ model: Lane }] }] }).then(res => {
                            expect(res.paths).to.be.an('array').with.lengthOf(1)
                            var paths = res.paths[0]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(1)
                            var ths = paths.lanes.map(l => l.lane_th)
                            expect(ths).to.have.members([2])
                            done()
                        }).catch(err => {
                            done(err)
                        })
                    })
                })
            })
        })

        describe('1 -> 2', () => {
            var segmentId = null
            before(async () => {
                var newSegment = {
                    segment_start: 'Segment Start',
                    segment_end: 'Segment End',
                    segment_paths: 1,
                    segment_density: 0,
                    segment_road_id: roadId
                }

                var segment = await Segment.create(newSegment)
                segmentId = segment.segment_id
            })

            after(() => {
                Segment.destroy({ where: { segment_id: segmentId } })
            })

            describe('First Condition', () => {
                var pathId = null
                beforeEach(async () => {
                    await Segment.update({ segment_paths: 1 }, { where: { segment_id: segmentId } })

                    var newPath = {
                        path_th: 0,
                        path_lanes: 4,
                        path_segment_id: segmentId
                    }

                    var path = await Path.create(newPath)
                    pathId = path.path_id

                    var newLanes = [
                        {
                            lane_th: 0,
                            lane_width: 3.15,
                            lane_cap: 123,
                            lane_path_id: pathId
                        },
                        {
                            lane_th: 1,
                            lane_width: 3.15,
                            lane_cap: 123,
                            lane_path_id: pathId
                        }
                    ]

                    await Lane.bulkCreate(newLanes)
                })

                afterEach(async () => {
                    console.log("after", pathId)
                    await Path.destroy({ where: { path_segment_id: segmentId } })
                })

                it('4 -> 2,2', (done) => {
                    var header = {
                        'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                        'Content-Type': 'application/json'
                    }

                    var segmentData = {
                        start: 'Segment Start',
                        end: 'Segment End',
                        paths: 2,
                        density: 2,
                        firstPath: {
                            id: pathId,
                            th: 0,
                            lanes: 2
                        },
                        secondPath: {
                            th: 1,
                            lanes: 2
                        }
                    }

                    request(app).post('/api/segment/edit/' + segmentId).set(header).send(segmentData).end((err, res) => {
                        if (err) done(err)
                        expect(res.statusCode).to.equal(200)
                        expect(res.body).to.have.property('status').to.equal(true)
                        Segment.findOne({ where: { segment_road_id: roadId }, include: [{ model: Path, include: [{ model: Lane }] }], order: [[Path, 'path_id', 'ASC']] }).then(res => {
                            expect(res.paths).to.be.an('array').with.lengthOf(2)

                            var paths = res.paths[0]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(2)
                            var ths = paths.lanes.map(l => l.lane_th)
                            expect(ths).to.have.members([0, 1])

                            paths = res.paths[1]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(0)
                            done()
                        }).catch(err => {
                            done(err)
                        })
                    })
                })

                it('4 -> 1,3', (done) => {
                    var header = {
                        'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                        'Content-Type': 'application/json'
                    }

                    var segmentData = {
                        start: 'Segment Start',
                        end: 'Segment End',
                        paths: 2,
                        density: 2,
                        firstPath: {
                            id: pathId,
                            th: 0,
                            lanes: 1
                        },
                        secondPath: {
                            th: 1,
                            lanes: 3
                        }
                    }

                    request(app).post('/api/segment/edit/' + segmentId).set(header).send(segmentData).end((err, res) => {
                        if (err) done(err)
                        expect(res.statusCode).to.equal(200)
                        expect(res.body).to.have.property('status').to.equal(true)
                        Segment.findOne({ where: { segment_road_id: roadId }, include: [{ model: Path, include: [{ model: Lane }] }], order: [[Path, 'path_id', 'ASC']] }).then(res => {
                            expect(res.paths).to.be.an('array').with.lengthOf(2)

                            var paths = res.paths[0]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(1)
                            var ths = paths.lanes.map(l => l.lane_th)
                            expect(ths).to.have.members([0])

                            paths = res.paths[1]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(1)
                            ths = paths.lanes.map(l => l.lane_th)
                            expect(ths).to.have.members([0])
                            done()
                        }).catch(err => {
                            done(err)
                        })
                    })
                })
            })

            describe('Second Condition', () => {
                var pathId = null
                beforeEach(async () => {
                    console.log("beforeEach 0")
                    await Segment.update({ segment_paths: 1 }, { where: { segment_id: segmentId } })

                    var newPath = {
                        path_th: 0,
                        path_lanes: 4,
                        path_segment_id: segmentId
                    }

                    var path = await Path.create(newPath)
                    pathId = path.path_id
                    console.log("beforeEach 1")
                    var newLanes = [
                        {
                            lane_th: 0,
                            lane_width: 3.15,
                            lane_cap: 123,
                            lane_path_id: pathId
                        },
                        {
                            lane_th: 2,
                            lane_width: 3.15,
                            lane_cap: 123,
                            lane_path_id: pathId
                        }
                    ]

                    await Lane.bulkCreate(newLanes)
                    console.log("beforeEach 2")
                })

                afterEach(() => {
                    console.log("afterEach", pathId)
                    Path.destroy({ where: { path_segment_id: segmentId } })
                })

                it('4 -> 2,2', (done) => {
                    var header = {
                        'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                        'Content-Type': 'application/json'
                    }

                    var segmentData = {
                        start: 'Segment Start',
                        end: 'Segment End',
                        paths: 2,
                        density: 2,
                        firstPath: {
                            id: pathId,
                            th: 0,
                            lanes: 2
                        },
                        secondPath: {
                            th: 1,
                            lanes: 2
                        }
                    }

                    request(app).post('/api/segment/edit/' + segmentId).set(header).send(segmentData).end((err, res) => {
                        if (err) done(err)
                        expect(res.statusCode).to.equal(200)
                        expect(res.body).to.have.property('status').to.equal(true)
                        Segment.findOne({ where: { segment_road_id: roadId }, include: [{ model: Path, include: [{ model: Lane }] }], order: [[Path, 'path_id', 'ASC']] }).then(res => {
                            expect(res.paths).to.be.an('array').with.lengthOf(2)

                            var paths = res.paths[0]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(1)
                            var ths = paths.lanes.map(l => l.lane_th)
                            expect(ths).to.have.members([0])

                            paths = res.paths[1]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(1)
                            ths = paths.lanes.map(l => l.lane_th)
                            expect(ths).to.have.members([0])
                            done()
                        }).catch(err => {
                            done(err)
                        })
                    })
                })

                it('4 -> 1,3', (done) => {
                    var header = {
                        'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                        'Content-Type': 'application/json'
                    }

                    var segmentData = {
                        start: 'Segment Start',
                        end: 'Segment End',
                        paths: 2,
                        density: 2,
                        firstPath: {
                            id: pathId,
                            th: 0,
                            lanes: 1
                        },
                        secondPath: {
                            th: 1,
                            lanes: 3
                        }
                    }

                    request(app).post('/api/segment/edit/' + segmentId).set(header).send(segmentData).end((err, res) => {
                        if (err) done(err)
                        expect(res.statusCode).to.equal(200)
                        expect(res.body).to.have.property('status').to.equal(true)
                        Segment.findOne({ where: { segment_road_id: roadId }, include: [{ model: Path, include: [{ model: Lane }] }], order: [[Path, 'path_id', 'ASC']] }).then(res => {
                            expect(res.paths).to.be.an('array').with.lengthOf(2)

                            var paths = res.paths[0]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(1)
                            var ths = paths.lanes.map(l => l.lane_th)
                            expect(ths).to.have.members([0])

                            paths = res.paths[1]
                            // console.log("4 -> 1,3", paths.lanes)
                            expect(paths.lanes).to.be.an('array').with.lengthOf(1)
                            ths = paths.lanes.map(l => l.lane_th)
                            expect(ths).to.have.members([1])
                            done()
                        }).catch(err => {
                            done(err)
                        })
                    })
                })

                it('4 -> 3,1', (done) => {
                    var header = {
                        'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                        'Content-Type': 'application/json'
                    }

                    var segmentData = {
                        start: 'Segment Start',
                        end: 'Segment End',
                        paths: 2,
                        density: 2,
                        firstPath: {
                            id: pathId,
                            th: 0,
                            lanes: 3
                        },
                        secondPath: {
                            th: 1,
                            lanes: 1
                        }
                    }

                    request(app).post('/api/segment/edit/' + segmentId).set(header).send(segmentData).end((err, res) => {
                        if (err) done(err)
                        expect(res.statusCode).to.equal(200)
                        expect(res.body).to.have.property('status').to.equal(true)
                        Segment.findOne({ where: { segment_road_id: roadId }, include: [{ model: Path, include: [{ model: Lane }] }], order: [[Path, 'path_id', 'ASC']] }).then(res => {
                            expect(res.paths).to.be.an('array').with.lengthOf(2)

                            var paths = res.paths[0]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(2)
                            var ths = paths.lanes.map(l => l.lane_th)
                            expect(ths).to.have.members([0, 2])

                            paths = res.paths[1]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(0)
                            done()
                        }).catch(err => {
                            done(err)
                        })
                    })
                })
            })

            describe('Third Condition', () => {
                var pathId = null
                beforeEach(async () => {
                    await Segment.update({ segment_paths: 1 }, { where: { segment_id: segmentId } })

                    var newPath = {
                        path_th: 0,
                        path_lanes: 4,
                        path_segment_id: segmentId
                    }

                    var path = await Path.create(newPath)
                    pathId = path.path_id

                    var newLanes = [
                        {
                            lane_th: 0,
                            lane_width: 3.15,
                            lane_cap: 123,
                            lane_path_id: pathId
                        },
                        {
                            lane_th: 3,
                            lane_width: 3.15,
                            lane_cap: 123,
                            lane_path_id: pathId
                        }
                    ]

                    await Lane.bulkCreate(newLanes)
                })

                afterEach(() => {
                    console.log("afterEach", pathId)
                    Path.destroy({ where: { path_segment_id: segmentId } })
                })

                it('4 -> 2,2', (done) => {
                    var header = {
                        'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                        'Content-Type': 'application/json'
                    }

                    var segmentData = {
                        start: 'Segment Start',
                        end: 'Segment End',
                        paths: 2,
                        density: 2,
                        firstPath: {
                            id: pathId,
                            th: 0,
                            lanes: 2
                        },
                        secondPath: {
                            th: 1,
                            lanes: 2
                        }
                    }

                    request(app).post('/api/segment/edit/' + segmentId).set(header).send(segmentData).end((err, res) => {
                        if (err) done(err)
                        expect(res.statusCode).to.equal(200)
                        expect(res.body).to.have.property('status').to.equal(true)
                        Segment.findOne({ where: { segment_road_id: roadId }, include: [{ model: Path, include: [{ model: Lane }] }], order: [[Path, 'path_id', 'ASC']] }).then(res => {
                            expect(res.paths).to.be.an('array').with.lengthOf(2)

                            var paths = res.paths[0]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(1)
                            var ths = paths.lanes.map(l => l.lane_th)
                            expect(ths).to.have.members([0])

                            paths = res.paths[1]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(1)
                            ths = paths.lanes.map(l => l.lane_th)
                            expect(ths).to.have.members([1])
                            done()
                        }).catch(err => {
                            done(err)
                        })
                    })
                })

                it('4 -> 1,3', (done) => {
                    var header = {
                        'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                        'Content-Type': 'application/json'
                    }

                    var segmentData = {
                        start: 'Segment Start',
                        end: 'Segment End',
                        paths: 2,
                        density: 2,
                        firstPath: {
                            id: pathId,
                            th: 0,
                            lanes: 1
                        },
                        secondPath: {
                            th: 1,
                            lanes: 3
                        }
                    }

                    request(app).post('/api/segment/edit/' + segmentId).set(header).send(segmentData).end((err, res) => {
                        if (err) done(err)
                        expect(res.statusCode).to.equal(200)
                        expect(res.body).to.have.property('status').to.equal(true)
                        Segment.findOne({ where: { segment_road_id: roadId }, include: [{ model: Path, include: [{ model: Lane }] }], order: [[Path, 'path_id', 'ASC']] }).then(res => {
                            expect(res.paths).to.be.an('array').with.lengthOf(2)

                            var paths = res.paths[0]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(1)
                            var ths = paths.lanes.map(l => l.lane_th)
                            expect(ths).to.have.members([0])

                            paths = res.paths[1]
                            console.log("4 -> 1,3", paths.lanes)
                            expect(paths.lanes).to.be.an('array').with.lengthOf(1)
                            ths = paths.lanes.map(l => l.lane_th)
                            expect(ths).to.have.members([2])
                            done()
                        }).catch(err => {
                            done(err)
                        })
                    })
                })

                it('4 -> 3,1', (done) => {
                    var header = {
                        'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                        'Content-Type': 'application/json'
                    }

                    var segmentData = {
                        start: 'Segment Start',
                        end: 'Segment End',
                        paths: 2,
                        density: 2,
                        firstPath: {
                            id: pathId,
                            th: 0,
                            lanes: 3
                        },
                        secondPath: {
                            th: 1,
                            lanes: 1
                        }
                    }

                    request(app).post('/api/segment/edit/' + segmentId).set(header).send(segmentData).end((err, res) => {
                        if (err) done(err)
                        expect(res.statusCode).to.equal(200)
                        expect(res.body).to.have.property('status').to.equal(true)
                        Segment.findOne({ where: { segment_road_id: roadId }, include: [{ model: Path, include: [{ model: Lane }] }], order: [[Path, 'path_id', 'ASC']] }).then(res => {
                            expect(res.paths).to.be.an('array').with.lengthOf(2)

                            var paths = res.paths[0]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(1)
                            var ths = paths.lanes.map(l => l.lane_th)
                            expect(ths).to.have.members([0])

                            paths = res.paths[1]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(1)
                            ths = paths.lanes.map(l => l.lane_th)
                            expect(ths).to.have.members([0])
                            done()
                        }).catch(err => {
                            done(err)
                        })
                    })
                })
            })

            describe('Fourth Condition', () => {
                var pathId = null
                beforeEach(async () => {
                    console.log('beforeEach')
                    await Segment.update({ segment_paths: 1 }, { where: { segment_id: segmentId } })

                    var newPath = {
                        path_th: 0,
                        path_lanes: 4,
                        path_segment_id: segmentId
                    }

                    var path = await Path.create(newPath)
                    pathId = path.path_id

                    var newLanes = [
                        {
                            lane_th: 1,
                            lane_width: 3.15,
                            lane_cap: 123,
                            lane_path_id: pathId
                        },
                        {
                            lane_th: 2,
                            lane_width: 3.15,
                            lane_cap: 123,
                            lane_path_id: pathId
                        }
                    ]

                    await Lane.bulkCreate(newLanes)
                })

                afterEach(() => {
                    console.log("afterEach", pathId)
                    Path.destroy({ where: { path_segment_id: segmentId } })
                })

                it('4 -> 2,2', (done) => {
                    var header = {
                        'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                        'Content-Type': 'application/json'
                    }

                    var segmentData = {
                        start: 'Segment Start',
                        end: 'Segment End',
                        paths: 2,
                        density: 2,
                        firstPath: {
                            id: pathId,
                            th: 0,
                            lanes: 2
                        },
                        secondPath: {
                            th: 1,
                            lanes: 2
                        }
                    }

                    request(app).post('/api/segment/edit/' + segmentId).set(header).send(segmentData).end((err, res) => {
                        if (err) done(err)
                        expect(res.statusCode).to.equal(200)
                        expect(res.body).to.have.property('status').to.equal(true)
                        Segment.findOne({ where: { segment_road_id: roadId }, include: [{ model: Path, include: [{ model: Lane }] }], order: [[Path, 'path_id', 'ASC']] }).then(res => {
                            expect(res.paths).to.be.an('array').with.lengthOf(2)

                            var paths = res.paths[0]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(1)
                            var ths = paths.lanes.map(l => l.lane_th)
                            expect(ths).to.have.members([1])

                            paths = res.paths[1]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(1)
                            ths = paths.lanes.map(l => l.lane_th)
                            expect(ths).to.have.members([0])
                            done()
                        }).catch(err => {
                            done(err)
                        })
                    })
                })

                it('4 -> 1,3', (done) => {
                    var header = {
                        'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                        'Content-Type': 'application/json'
                    }

                    var segmentData = {
                        start: 'Segment Start',
                        end: 'Segment End',
                        paths: 2,
                        density: 2,
                        firstPath: {
                            id: pathId,
                            th: 0,
                            lanes: 1
                        },
                        secondPath: {
                            th: 1,
                            lanes: 3
                        }
                    }

                    request(app).post('/api/segment/edit/' + segmentId).set(header).send(segmentData).end((err, res) => {
                        if (err) done(err)
                        expect(res.statusCode).to.equal(200)
                        expect(res.body).to.have.property('status').to.equal(true)
                        Segment.findOne({ where: { segment_road_id: roadId }, include: [{ model: Path, include: [{ model: Lane }] }], order: [[Path, 'path_id', 'ASC']] }).then(res => {
                            expect(res.paths).to.be.an('array').with.lengthOf(2)

                            var paths = res.paths[0]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(0)

                            paths = res.paths[1]
                            console.log("4 -> 1,3", paths.lanes)
                            expect(paths.lanes).to.be.an('array').with.lengthOf(2)
                            var ths = paths.lanes.map(l => l.lane_th)
                            expect(ths).to.have.members([0, 1])
                            done()
                        }).catch(err => {
                            done(err)
                        })
                    })
                })

                it('4 -> 3,1', (done) => {
                    var header = {
                        'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                        'Content-Type': 'application/json'
                    }

                    var segmentData = {
                        start: 'Segment Start',
                        end: 'Segment End',
                        paths: 2,
                        density: 2,
                        firstPath: {
                            id: pathId,
                            th: 0,
                            lanes: 3
                        },
                        secondPath: {
                            th: 1,
                            lanes: 1
                        }
                    }

                    request(app).post('/api/segment/edit/' + segmentId).set(header).send(segmentData).end((err, res) => {
                        if (err) done(err)
                        expect(res.statusCode).to.equal(200)
                        expect(res.body).to.have.property('status').to.equal(true)
                        Segment.findOne({ where: { segment_road_id: roadId }, include: [{ model: Path, include: [{ model: Lane }] }], order: [[Path, 'path_id', 'ASC']] }).then(res => {
                            expect(res.paths).to.be.an('array').with.lengthOf(2)

                            var paths = res.paths[0]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(2)
                            var ths = paths.lanes.map(l => l.lane_th)
                            expect(ths).to.have.members([1, 2])

                            paths = res.paths[1]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(0)
                            done()
                        }).catch(err => {
                            done(err)
                        })
                    })
                })
            })

            describe('Fifth Condition', () => {
                var pathId = null
                beforeEach(async () => {
                    await Segment.update({ segment_paths: 1 }, { where: { segment_id: segmentId } })

                    var newPath = {
                        path_th: 0,
                        path_lanes: 4,
                        path_segment_id: segmentId
                    }

                    var path = await Path.create(newPath)
                    pathId = path.path_id

                    var newLanes = [
                        {
                            lane_th: 1,
                            lane_width: 3.15,
                            lane_cap: 123,
                            lane_path_id: pathId
                        },
                        {
                            lane_th: 3,
                            lane_width: 3.15,
                            lane_cap: 123,
                            lane_path_id: pathId
                        }
                    ]

                    await Lane.bulkCreate(newLanes)
                })

                afterEach(() => {
                    console.log("afterEach", pathId)
                    Path.destroy({ where: { path_segment_id: segmentId } })
                })

                it('4 -> 2,2', (done) => {
                    var header = {
                        'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                        'Content-Type': 'application/json'
                    }

                    var segmentData = {
                        start: 'Segment Start',
                        end: 'Segment End',
                        paths: 2,
                        density: 2,
                        firstPath: {
                            id: pathId,
                            th: 0,
                            lanes: 2
                        },
                        secondPath: {
                            th: 1,
                            lanes: 2
                        }
                    }

                    request(app).post('/api/segment/edit/' + segmentId).set(header).send(segmentData).end((err, res) => {
                        if (err) done(err)
                        expect(res.statusCode).to.equal(200)
                        expect(res.body).to.have.property('status').to.equal(true)
                        Segment.findOne({ where: { segment_road_id: roadId }, include: [{ model: Path, include: [{ model: Lane }] }], order: [[Path, 'path_id', 'ASC']] }).then(res => {
                            expect(res.paths).to.be.an('array').with.lengthOf(2)

                            var paths = res.paths[0]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(1)
                            var ths = paths.lanes.map(l => l.lane_th)
                            expect(ths).to.have.members([1])

                            paths = res.paths[1]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(1)
                            ths = paths.lanes.map(l => l.lane_th)
                            expect(ths).to.have.members([1])
                            done()
                        }).catch(err => {
                            done(err)
                        })
                    })
                })

                it('4 -> 1,3', (done) => {
                    var header = {
                        'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                        'Content-Type': 'application/json'
                    }

                    var segmentData = {
                        start: 'Segment Start',
                        end: 'Segment End',
                        paths: 2,
                        density: 2,
                        firstPath: {
                            id: pathId,
                            th: 0,
                            lanes: 1
                        },
                        secondPath: {
                            th: 1,
                            lanes: 3
                        }
                    }

                    request(app).post('/api/segment/edit/' + segmentId).set(header).send(segmentData).end((err, res) => {
                        if (err) done(err)
                        expect(res.statusCode).to.equal(200)
                        expect(res.body).to.have.property('status').to.equal(true)
                        Segment.findOne({ where: { segment_road_id: roadId }, include: [{ model: Path, include: [{ model: Lane }] }], order: [[Path, 'path_id', 'ASC']] }).then(res => {
                            expect(res.paths).to.be.an('array').with.lengthOf(2)

                            var paths = res.paths[0]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(0)

                            paths = res.paths[1]
                            // console.log("4 -> 1,3", paths.lanes)
                            expect(paths.lanes).to.be.an('array').with.lengthOf(2)
                            var ths = paths.lanes.map(l => l.lane_th)
                            expect(ths).to.have.members([0, 2])
                            done()
                        }).catch(err => {
                            done(err)
                        })
                    })
                })

                it('4 -> 3,1', (done) => {
                    var header = {
                        'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                        'Content-Type': 'application/json'
                    }

                    var segmentData = {
                        start: 'Segment Start',
                        end: 'Segment End',
                        paths: 2,
                        density: 2,
                        firstPath: {
                            id: pathId,
                            th: 0,
                            lanes: 3
                        },
                        secondPath: {
                            th: 1,
                            lanes: 1
                        }
                    }

                    request(app).post('/api/segment/edit/' + segmentId).set(header).send(segmentData).end((err, res) => {
                        if (err) done(err)
                        expect(res.statusCode).to.equal(200)
                        expect(res.body).to.have.property('status').to.equal(true)
                        Segment.findOne({ where: { segment_road_id: roadId }, include: [{ model: Path, include: [{ model: Lane }] }], order: [[Path, 'path_id', 'ASC']] }).then(res => {
                            expect(res.paths).to.be.an('array').with.lengthOf(2)

                            var paths = res.paths[0]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(1)
                            var ths = paths.lanes.map(l => l.lane_th)
                            expect(ths).to.have.members([1])

                            paths = res.paths[1]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(1)
                            ths = paths.lanes.map(l => l.lane_th)
                            expect(ths).to.have.members([0])
                            done()
                        }).catch(err => {
                            done(err)
                        })
                    })
                })
            })

            describe('Sixth Condition', () => {
                var pathId = null
                beforeEach(async () => {
                    await Segment.update({ segment_paths: 1 }, { where: { segment_id: segmentId } })

                    var newPath = {
                        path_th: 0,
                        path_lanes: 4,
                        path_segment_id: segmentId
                    }

                    var path = await Path.create(newPath)
                    pathId = path.path_id

                    var newLanes = [
                        {
                            lane_th: 2,
                            lane_width: 3.15,
                            lane_cap: 123,
                            lane_path_id: pathId
                        },
                        {
                            lane_th: 3,
                            lane_width: 3.15,
                            lane_cap: 123,
                            lane_path_id: pathId
                        }
                    ]

                    await Lane.bulkCreate(newLanes)
                })

                afterEach(() => {
                    console.log("afterEach", pathId)
                    Path.destroy({ where: { path_segment_id: segmentId } })
                })

                it('4 -> 2,2', (done) => {
                    var header = {
                        'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                        'Content-Type': 'application/json'
                    }

                    var segmentData = {
                        start: 'Segment Start',
                        end: 'Segment End',
                        paths: 2,
                        density: 2,
                        firstPath: {
                            id: pathId,
                            th: 0,
                            lanes: 2
                        },
                        secondPath: {
                            th: 1,
                            lanes: 2
                        }
                    }

                    request(app).post('/api/segment/edit/' + segmentId).set(header).send(segmentData).end((err, res) => {
                        if (err) done(err)
                        expect(res.statusCode).to.equal(200)
                        expect(res.body).to.have.property('status').to.equal(true)
                        Segment.findOne({ where: { segment_road_id: roadId }, include: [{ model: Path, include: [{ model: Lane }] }], order: [[Path, 'path_id', 'ASC']] }).then(res => {
                            expect(res.paths).to.be.an('array').with.lengthOf(2)
                            var paths = res.paths[0]
                            // console.log('4 -> 2,2', paths)
                            expect(paths.lanes).to.be.an('array').with.lengthOf(0)

                            paths = res.paths[1]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(2)
                            var ths = paths.lanes.map(l => l.lane_th)
                            expect(ths).to.have.members([0, 1])
                            done()
                        }).catch(err => {
                            done(err)
                        })
                    })
                })

                it('4 -> 1,3', (done) => {
                    var header = {
                        'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                        'Content-Type': 'application/json'
                    }

                    var segmentData = {
                        start: 'Segment Start',
                        end: 'Segment End',
                        paths: 2,
                        density: 2,
                        firstPath: {
                            id: pathId,
                            th: 0,
                            lanes: 1
                        },
                        secondPath: {
                            th: 1,
                            lanes: 3
                        }
                    }

                    request(app).post('/api/segment/edit/' + segmentId).set(header).send(segmentData).end((err, res) => {
                        if (err) done(err)
                        expect(res.statusCode).to.equal(200)
                        expect(res.body).to.have.property('status').to.equal(true)
                        Segment.findOne({ where: { segment_road_id: roadId }, include: [{ model: Path, include: [{ model: Lane }] }], order: [[Path, 'path_id', 'ASC']] }).then(res => {
                            expect(res.paths).to.be.an('array').with.lengthOf(2)

                            var paths = res.paths[0]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(0)

                            paths = res.paths[1]
                            // console.log("4 -> 1,3", paths.lanes)
                            expect(paths.lanes).to.be.an('array').with.lengthOf(2)
                            var ths = paths.lanes.map(l => l.lane_th)
                            expect(ths).to.have.members([1, 2])
                            done()
                        }).catch(err => {
                            done(err)
                        })
                    })
                })

                it('4 -> 3,1', (done) => {
                    var header = {
                        'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                        'Content-Type': 'application/json'
                    }

                    var segmentData = {
                        start: 'Segment Start',
                        end: 'Segment End',
                        paths: 2,
                        density: 2,
                        firstPath: {
                            id: pathId,
                            th: 0,
                            lanes: 3
                        },
                        secondPath: {
                            th: 1,
                            lanes: 1
                        }
                    }

                    request(app).post('/api/segment/edit/' + segmentId).set(header).send(segmentData).end((err, res) => {
                        if (err) done(err)
                        expect(res.statusCode).to.equal(200)
                        expect(res.body).to.have.property('status').to.equal(true)
                        Segment.findOne({ where: { segment_road_id: roadId }, include: [{ model: Path, include: [{ model: Lane }] }], order: [[Path, 'path_id', 'ASC']] }).then(res => {
                            expect(res.paths).to.be.an('array').with.lengthOf(2)

                            var paths = res.paths[0]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(1)
                            var ths = paths.lanes.map(l => l.lane_th)
                            expect(ths).to.have.members([2])

                            paths = res.paths[1]
                            expect(paths.lanes).to.be.an('array').with.lengthOf(1)
                            ths = paths.lanes.map(l => l.lane_th)
                            expect(ths).to.have.members([0])
                            done()
                        }).catch(err => {
                            done(err)
                        })
                    })
                })
            })
        })
    })
})