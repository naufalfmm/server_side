'use strict'

var request = require('supertest'),
    expect = require('chai').expect;
var app = require('../app');
var Road = require('../app/models/road');

describe('Testing Road Controller', () => {
    describe('POST Add Road', () => {
        describe('Succeed Add', () => {
            after(() => {
                console.log('after')
                Road.destroy({
                    where: {}
                })
            })

            it('Full Data', (done) => {
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                var roadData = {
                    name: 'Road Name',
                    pop_range: 0,
                    segment: [
                        {
                            start: "Segment Start",
                            end: "Segment End",
                            paths: 1,
                            density: 1,
                            firstPath: {
                                th: 0,
                                lanes: 2
                            }
                        }
                    ]
                }

                request(app).post('/api/road/add').set(header).send(roadData).end((err, res) => {
                    if (err) done(err)
                    expect(res.statusCode).to.equal(201)
                    expect(res.body).to.have.property('status').to.equal(true)
                    done()
                })
            })

            it('No Segment Data', (done) => {
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                var roadData = {
                    name: 'Road Name',
                    pop_range: 0,
                }

                request(app).post('/api/road/add').set(header).send(roadData).end((err, res) => {
                    if (err) done(err)
                    expect(res.statusCode).to.equal(201)
                    expect(res.body).to.have.property('status').to.equal(true)
                    done()
                })
            })
        })

        describe('Failed Add', () => {
            it('Undefined', (done) => {
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                var roadData = {
                    name: 'Road Name',
                    pop_range: 0
                }

                var prop = ['name', 'pop_range']
                var propIdx = Math.floor(Math.random() * 2)

                delete roadData[prop[propIdx]]

                request(app).post('/api/road/add').set(header).send(roadData).end((err, res) => {
                    if (err) done(err)
                    expect(res.statusCode).to.equal(400)
                    expect(res.body).to.have.property('status').to.equal(false)
                    done()
                })
            })

            it('Empty', (done) => {
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                var roadData = {
                    name: 'Road Name',
                    pop_range: 0
                }

                var prop = ['name', 'pop_range']
                var propIdx = Math.floor(Math.random() * 2)

                roadData[prop[propIdx]] = ''

                request(app).post('/api/road/add').set(header).send(roadData).end((err, res) => {
                    if (err) done(err)
                    expect(res.statusCode).to.equal(400)
                    expect(res.body).to.have.property('status').to.equal(false)
                    done()
                })
            })
        })
    })

    describe('POST Edit Road', () => {
        describe('Succeed Edit', () => {
            var roadId = null
            before(async () => {
                var newRoad = {
                    road_name: "Road Name",
                    road_population_range: 0
                }

                var road = await Road.create(newRoad)
                roadId = road.road_id
            })

            after(() => {
                console.log('after')
                Road.destroy({
                    where: {road_id: roadId}
                })
            })

            it('Full Data', (done) => {
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                var roadData = {
                    name: 'Road Name Edit',
                    pop_range: 1,
                }

                request(app).post('/api/road/edit/' + roadId).set(header).send(roadData).end((err, res) => {
                    if (err) done(err)
                    expect(res.statusCode).to.equal(201)
                    expect(res.body).to.have.property('status').to.equal(true)
                    done()
                })
            })
        })

        describe('Failed Edit', () => {
            var roadId = null
            before(async () => {
                var newRoad = {
                    road_name: "Road Name",
                    road_population_range: 0
                }

                var road = await Road.create(newRoad)
                roadId = road.road_id
            })

            after(() => {
                console.log('after')
                Road.destroy({
                    where: { road_id: roadId }
                })
            })

            it('Undefined', (done) => {
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                var roadData = {
                    name: 'Road Name Edit',
                    pop_range: 1
                }

                var prop = ['name', 'pop_range']
                var propIdx = Math.floor(Math.random() * 2)

                delete roadData[prop[propIdx]]

                request(app).post('/api/road/add').set(header).send(roadData).end((err, res) => {
                    if (err) done(err)
                    expect(res.statusCode).to.equal(400)
                    expect(res.body).to.have.property('status').to.equal(false)
                    done()
                })
            })

            it('Empty', (done) => {
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                var roadData = {
                    name: 'Road Name Edit',
                    pop_range: 1
                }

                var prop = ['name', 'pop_range']
                var propIdx = Math.floor(Math.random() * 2)

                roadData[prop[propIdx]] = ''

                request(app).post('/api/road/add').set(header).send(roadData).end((err, res) => {
                    if (err) done(err)
                    expect(res.statusCode).to.equal(400)
                    expect(res.body).to.have.property('status').to.equal(false)
                    done()
                })
            })
        })
    })

    describe('DELETE Delete Road', () => {
        describe('Succeed Delete', () => {
            var roadId = null
            before(async () => {
                var newRoad = {
                    road_name: "Road Name",
                    road_population_range: 0
                }

                var road = await Road.create(newRoad)
                roadId = road.road_id
            })

            it('Delete', (done) => {
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                request(app).delete('/api/road/delete/' + roadId).set(header).end((err, res) => {
                    if (err) done(err)
                    expect(res.statusCode).to.equal(200)
                    expect(res.body).to.have.property('status').to.equal(true)
                    done()
                })
            })
        })

        describe('Failed Delete', () => {
            it('No Road Id Exist', (done) => {
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                request(app).delete('/api/road/delete/' + 0).set(header).end((err, res) => {
                    if (err) done(err)
                    expect(res.statusCode).to.equal(403)
                    expect(res.body).to.have.property('status').to.equal(false)
                    done()
                })
            })
        })
    })

    describe('GET Get Road', () => {
        var roadId = null
        before(async () => {
            var newRoad = {
                road_name: "Road Name",
                road_population_range: 0
            }

            var road = await Road.create(newRoad)
            roadId = road.road_id
        })

        after(() => {
            console.log('after')
            Road.destroy({
                where: { road_id: roadId }
            })
        })

        describe('Succeed Get', () => {
            it('Get', (done) => {
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                request(app).get('/api/road/' + roadId).set(header).end((err, res) => {
                    if (err) done(err)
                    expect(res.statusCode).to.equal(200)
                    expect(res.body).to.have.property('status').to.equal(true)
                    expect(res.body).to.have.property('data')
                    done()
                })
            })
        })

        describe('Failed Get', () => {
            it('Not Found', (done) => {
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                request(app).get('/api/road/' + (roadId + 1)).set(header).end((err, res) => {
                    if (err) done(err)
                    expect(res.statusCode).to.equal(404)
                    expect(res.body).to.have.property('status').to.equal(false)
                    done()
                })
            })
        })
    })

    describe('GET Get Road All', () => {
        describe('Succeed Get All', () => {
            before(async () => {
                var newRoads = [
                    {
                        road_name: "Road Name A",
                        road_population_range: 0
                    },
                    {
                        road_name: "Road Name B",
                        road_population_range: 1
                    },
                    {
                        road_name: "Road Name C",
                        road_population_range: 2
                    }
                ]

                await Road.bulkCreate(newRoads)
            })

            after(() => {
                console.log('after')
                Road.destroy({
                    where: {}
                })
            })

            it('Get All', (done) => {
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                request(app).get('/api/road/all').set(header).end((err, res) => {
                    if (err) done(err)
                    expect(res.statusCode).to.equal(200)
                    expect(res.body).to.have.property('status').to.equal(true)
                    expect(res.body).to.have.property('road_data').with.lengthOf.at.least(3)
                    expect(res.body).to.have.property('road_full').with.lengthOf.at.least(3)
                    done()
                })
            })
        })

        describe('Failed Get All', () => {
            it('Empty', (done) => {
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                request(app).get('/api/road/all').set(header).end((err, res) => {
                    if (err) done(err)
                    console.log(res.body)
                    expect(res.statusCode).to.equal(404)
                    expect(res.body).to.have.property('status').to.equal(false)
                    expect(res.body).to.not.have.property('road_data')
                    expect(res.body).to.not.have.property('road_full')
                    done()
                })
            })
        })
    })
})