'use strict';

var request = require('supertest'),
    path = require('path'),
    expect = require('chai').expect;
var app = require('../app');
var db = require('../app/services/database'),
    KeygenAES = require('../app/lib/encrypt'),
    defConst = require('../app/const/defconst'),
    ff = require('../app/lib/file');
var Road = require('../app/models/road'),
    Segment = require('../app/models/segment'),
    Path = require('../app/models/path'),
    Lane = require('../app/models/lane'),
    Node = require('../app/models/node');

describe('Testing Node Controller', () => {
    var roadId, segmentId, pathId = null
    before(async () => {
        var newRoad = {
            road_name: "Road Name",
            road_population_range: 0
        }

        var road = await Road.create(newRoad)
        roadId = road.road_id

        var newSegment = {
            segment_start: 'Segment Start',
            segment_end: 'Segment End',
            segment_paths: 2,
            segment_density: 0,
            segment_road_id: roadId
        }

        var segment = await Segment.create(newSegment)
        segmentId = segment.segment_id
    })

    after(() => {
        Road.destroy({ where: { road_id: roadId } })
    })

    describe('POST Login', () => {
        var pathId, laneId, nodeId = null
        before(async () => {
            var newPath = {
                path_th: 0,
                path_lanes: 2,
                path_segment_id: segmentId
            }

            var path = await Path.create(newPath)
            // console.log("beforeAllHook", path, path.path_id)
            pathId = path.path_id

            var newLane = {
                lane_th: 0,
                lane_width: 3.16,
                lane_cap: 123,
                lane_path_id: pathId
            }

            var lane = await Lane.create(newLane)
            laneId = lane.lane_id

            var newNode = {
                node_username: "node_" + laneId.toString(),
                node_key: await KeygenAES.generatekey(),
                node_lane_id: laneId
            }

            var node = await Node.create(newNode)
            nodeId = node.node_id
        })

        after(async () => {
            await Path.destroy({ where: { path_id: pathId } })
        })

        describe('Succeed Login', () => {
            it('Test', (done) => {
                var loginData = {
                    username: 'node_' + laneId.toString()
                }

                request(app).post('/api/node/login').send(loginData).end((err, res) => {
                    if (err) {
                        done(err)
                    }
                    expect(res.statusCode).to.equal(200)
                    expect(res.body).to.have.property('status').to.equal(true)
                    expect(res.body).to.have.property('token')
                    done()
                })
            })
        })

        describe('Failed Login', () => {
            it('No Username Found', (done) => {
                var loginData = {
                    username: 'node_0'
                }

                request(app).post('/api/node/login').send(loginData).end((err, res) => {
                    if (err) {
                        done(err)
                    }
                    expect(res.statusCode).to.equal(401)
                    expect(res.body).to.have.property('status').to.equal(false)
                    done()
                })
            })
        })
    })

    describe('POST Add Node', () => {
        describe('Succeed Add', () => {
            var pathId = null
            before(async () => {
                console.log("before succeed add")
                var newPath = {
                    path_th: 0,
                    path_lanes: 2,
                    path_segment_id: segmentId
                }

                var path = await Path.create(newPath)
                pathId = path.path_id
            })

            after(async () => {
                await Path.destroy({ where: { path_id: pathId } })
            })

            it('Test', (done) => {
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                var newNode = {
                    th: 0,
                    width: 3.15,
                    capacity: 123,
                    path_id: pathId
                }

                request(app).post('/api/node/add').set(header).send(newNode).end((err, res) => {
                    if (err) done(err)
                    expect(res.statusCode).to.equal(201)
                    expect(res.body).to.have.property('status').to.equal(true)
                    done()
                })
            })
        })

        describe('Failed Add', () => {
            var pathId = null
            before(async () => {
                console.log("before failed add")
                var newPath = {
                    path_th: 0,
                    path_lanes: 2,
                    path_segment_id: segmentId
                }

                var path = await Path.create(newPath)
                pathId = path.path_id
            })

            after(async () => {
                console.log("after failed add")
                await Path.destroy({ where: { path_id: pathId } })
            })

            it('Undefined', (done) => {
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                var newNode = {
                    th: 0,
                    width: 3.15,
                    capacity: 123,
                    path_id: pathId
                }

                var prop = ['th', 'width', 'capacity', 'path_id']
                var propIdx = Math.floor(Math.random() * 4)

                delete newNode[prop[propIdx]]

                request(app).post('/api/node/add').set(header).send(newNode).end((err, res) => {
                    if (err) done(err)
                    expect(res.statusCode).to.equal(400)
                    expect(res.body).to.have.property('status').to.equal(false)
                    done()
                })
            })

            it('Empty', (done) => {
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                var newNode = {
                    th: 0,
                    width: 3.15,
                    capacity: 123,
                    path_id: pathId
                }

                var prop = ['th', 'width', 'capacity', 'path_id']
                var propIdx = Math.floor(Math.random() * 4)

                newNode[prop[propIdx]] = ''

                request(app).post('/api/node/add').set(header).send(newNode).end((err, res) => {
                    if (err) done(err)
                    expect(res.statusCode).to.equal(400)
                    expect(res.body).to.have.property('status').to.equal(false)
                    done()
                })
            })
        })
    })

    describe('DELETE Delete Node', () => {
        var pathId, laneId, nodeId = null
        before(async () => {
            var newPath = {
                path_th: 0,
                path_lanes: 2,
                path_segment_id: segmentId
            }

            var path = await Path.create(newPath)
            // console.log("beforeAllHook", path, path.path_id)
            pathId = path.path_id
        })

        after(async () => {
            await Path.destroy({ where: { path_id: pathId } })
        })

        describe('Succeed Delete', () => {
            var laneId, nodeId = null
            before(async () => {
                var newLane = {
                    lane_th: 0,
                    lane_width: 3.16,
                    lane_cap: 123,
                    lane_path_id: pathId
                }

                var lane = await Lane.create(newLane)
                laneId = lane.lane_id

                var newNode = {
                    node_username: "node_" + laneId.toString(),
                    node_key: await KeygenAES.generatekey(),
                    node_lane_id: laneId
                }

                var node = await Node.create(newNode)
                nodeId = node.node_id
            })

            it('Test', (done) => {
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                request(app).delete('/api/node/delete/' + nodeId).set(header).end((err, res) => {
                    if (err) done(err)
                    expect(res.statusCode).to.equal(200)
                    expect(res.body).to.have.property('status').to.equal(true)
                    done()
                })
            })
        })

        describe('Failed Delete', () => {
            it('No Node Id Found', (done) => {
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                request(app).delete('/api/node/delete/1').set(header).end((err, res) => {
                    if (err) done(err)
                    expect(res.statusCode).to.equal(403)
                    expect(res.body).to.have.property('status').to.equal(false)
                    done()
                })
            })
        })
    })

    describe('GET Get All Node', () => {
        var pathsId = [0,0]
        before(async () => {
            var newPath = {
                path_th: 0,
                path_lanes: 2,
                path_segment_id: segmentId
            }

            var path = await Path.create(newPath)
            pathsId[0] = path.path_id

            newPath = {
                path_th: 1,
                path_lanes: 2,
                path_segment_id: segmentId
            }

            path = await Path.create(newPath)
            pathsId[1] = path.path_id

            var newLane = {
                lane_th: 0,
                lane_cap: 123,
                lane_width: 3.15,
                lane_path_id: pathsId[0]
            }

            var lane = await Lane.create(newLane)

            var newNode = {
                node_username: 'node_' + lane.lane_id.toString(),
                node_key: await KeygenAES.generatekey(),
                node_lane_id: lane.lane_id
            }

            await Node.create(newNode)

            newLane = {
                lane_th: 1,
                lane_cap: 123,
                lane_width: 3.15,
                lane_path_id: pathsId[0]
            }

            lane = await Lane.create(newLane)

            newNode = {
                node_username: 'node_' + lane.lane_id.toString(),
                node_key: await KeygenAES.generatekey(),
                node_lane_id: lane.lane_id
            }

            await Node.create(newNode)

            newLane = {
                lane_th: 0,
                lane_cap: 123,
                lane_width: 3.15,
                lane_path_id: pathsId[1]
            }

            lane = await Lane.create(newLane)

            newNode = {
                node_username: 'node_' + lane.lane_id.toString(),
                node_key: await KeygenAES.generatekey(),
                node_lane_id: lane.lane_id
            }

            await Node.create(newNode)
        })

        after(async () => {
            await Path.destroy({ where: { path_id: pathsId } })
        })

        describe('Succeed Get All', () => {
            it('Test', (done) => {
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                request(app).get('/api/node/all').set(header).end((err, res) => {
                    if (err) done(err)
                    expect(res.statusCode).to.equal(200)
                    expect(res.body).to.have.property('status').to.equal(true)
                    expect(res.body).to.have.property('node_data').with.lengthOf(3)
                    done()
                })
            })
        })

        describe('Failed Get All', () => {
            before(async () => {
                await Lane.destroy({ where: {} })
            })

            it('Test', (done) => {
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                request(app).get('/api/node/all').set(header).end((err, res) => {
                    if (err) done(err)
                    expect(res.statusCode).to.equal(404)
                    expect(res.body).to.have.property('status').to.equal(false)
                    done()
                })
            })
        })
    })

    describe('GET Get Node', () => {
        var pathId, nodeId = null
        before(async () => {
            var newPath = {
                path_th: 0,
                path_lanes: 2,
                path_segment_id: segmentId
            }

            var path = await Path.create(newPath)
            pathId = path.path_id

            var newLane = {
                lane_th: 0,
                lane_cap: 123,
                lane_width: 3.15,
                lane_path_id: pathId
            }

            var lane = await Lane.create(newLane)

            var newNode = {
                node_username: 'node_' + lane.lane_id.toString(),
                node_key: await KeygenAES.generatekey(),
                node_lane_id: lane.lane_id
            }

            var node = await Node.create(newNode)
            nodeId = node.node_id
        })

        after(async () => {
            await Path.destroy({ where: { path_id: pathId } })
        })

        describe('Succeed Get', () => {
            it('Test', (done) => {
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                request(app).get('/api/node/' + nodeId).set(header).end((err, res) => {
                    if (err) done(err)
                    expect(res.statusCode).to.equal(200)
                    expect(res.body).to.have.property('status').to.equal(true)
                    expect(res.body).to.have.property('node_data')
                    expect(res.body).to.have.property('segment_data')
                    done()
                })
            })
        })

        describe('Failed Get', () => {
            it('No Node Id Found', (done) => {
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                request(app).get('/api/node/' + 0).set(header).end((err, res) => {
                    if (err) done(err)
                    expect(res.statusCode).to.equal(404)
                    expect(res.body).to.have.property('status').to.equal(false)
                    done()
                })
            })
        })
    })

    var pathId, laneId, nodeId = null
    describe('POST Create Config', () => {
        before(async () => {
            var newPath = {
                path_th: 0,
                path_lanes: 2,
                path_segment_id: segmentId
            }

            var path = await Path.create(newPath)
            pathId = path.path_id

            var newLane = {
                lane_th: 0,
                lane_cap: 123,
                lane_width: 3.15,
                lane_path_id: pathId
            }

            var lane = await Lane.create(newLane)
            laneId = lane.lane_id

            var newNode = {
                node_username: 'node_' + laneId.toString(),
                node_key: await KeygenAES.generatekey(),
                node_lane_id: laneId
            }

            var node = await Node.create(newNode)
            nodeId = node.node_id
            // console.log("before create", nodeId, newNode, node)
        })

        describe('Succeed Create', () => {
            it('Test', (done) => {
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                request(app).post('/api/node/create/' + nodeId).set(header).end(async (err, res) => {
                    if (err) done(err)
                    expect(res.statusCode).to.equal(201)
                    expect(res.body).to.have.property('status').to.equal(true)
                    var file_path = defConst.pathUpload + path.sep + "node_" + laneId.toString() + path.sep + "config.py"
                    var checkFile = await ff.checkFile(file_path)
                    expect(checkFile).to.have.property('status').to.equal(true)
                    done()
                })
            })
        })

        describe('Failed Create', () => {
            it('No Node Id Found', (done) => {
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                request(app).post('/api/node/create/' + 0).set(header).end(async (err, res) => {
                    if (err) done(err)
                    expect(res.statusCode).to.equal(403)
                    expect(res.body).to.have.property('status').to.equal(false)
                    var file_path = defConst.pathUpload + path.sep + "node_0" + path.sep + "config.py"
                    var checkFile = await ff.checkFile(file_path)
                    expect(checkFile).to.have.property('status').to.equal(false)
                    done()
                })
            })
        })
    })

    describe('GET Download Config', () => {
        after(async () => {
            await Path.destroy({ where: { path_id: pathId } })
        })

        describe('Succeed Download', () => {
            it('Test', (done) => {
                console.log("Download")
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                request(app).get('/api/node/download/' + nodeId).set(header).end((err, res) => {
                    if (err) done(err)
                    console.log(res.body, res.body instanceof Buffer, res)
                    expect(res.statusCode).to.equal(200)
                    expect(res.body).to.be.instanceof(Buffer)
                    expect(res.type).to.equal('application/octet-stream')
                    done()
                })
            })
        })

        describe('Failed Download', () => {
            it('No Node Id Found', (done) => {
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                request(app).get('/api/node/' + 0).set(header).end((err, res) => {
                    if (err) done(err)
                    expect(res.statusCode).to.equal(404)
                    expect(res.body).to.have.property('status').to.equal(false)
                    done()
                })
            })
        })
    })
})