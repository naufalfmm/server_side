'use strict';

var LaneController = require('../app/controllers/laneController');
var Road = require('../app/models/road'),
    Segment = require('../app/models/segment'),
    Path = require('../app/models/path'),
    Lane = require('../app/models/lane');
var db = require('../app/services/database');
var expect = require('chai').expect;

describe('Test Lane Controller', () => {
    var roadId, segmentId = null
    before(async () => {
        var newRoad = {
            road_name: "Road Name",
            road_population_range: 0
        }

        var road = await Road.create(newRoad)
        roadId = road.road_id

        var newSegment = {
            segment_start: 'Segment Start',
            segment_end: 'Segment End',
            segment_paths: 1,
            segment_density: 0,
            segment_road_id: roadId
        }

        var segment = await Segment.create(newSegment)
        segmentId = segment.segment_id
    })

    after(() => {
        Road.destroy({ where: { road_id: roadId } })
    })

    describe('Add Lane', () => {
        describe('Succeed Add', () => {
            var pathId = null
            before(async () => {
                var newPath = {
                    path_th: 0,
                    path_lanes: 2,
                    path_segment_id: segmentId
                }

                var path = await Path.create(newPath)
                pathId = path.path_id
            })

            after(() => {
                Path.destroy({ where: { path_id: pathId } })
            })

            it('Full Data', (done) => {
                var newLane = {
                    th: 0,
                    width: 3.15,
                    capacity: 123,
                    path_id: pathId
                }

                LaneController.addLaneOff(newLane, null).then((res) => {
                    expect(res).to.have.property('status').to.equal(true)
                    expect(res).to.have.property('laneId').to.not.be.null
                    done()
                }).catch((err) => {
                    done(err)
                })
            })
        })

        describe('Failed Add', () => {
            var pathId = null
            before(async () => {
                var newPath = {
                    path_th: 0,
                    path_lanes: 2,
                    path_segment_id: segmentId
                }

                var path = await Path.create(newPath)
                pathId = path.path_id
            })

            after(() => {
                Path.destroy({ where: { path_id: pathId } })
            })

            it('Undefined', (done) => {
                var newLane = {
                    th: 0,
                    width: 3.15,
                    capacity: 123,
                    path_id: pathId
                }

                var prop = ['th', 'width', 'capacity', 'path_id']
                var propIdx = Math.floor(Math.random() * 4)

                delete newLane[prop[propIdx]]

                LaneController.addLaneOff(newLane, null).then((res) => {
                    expect(res).to.have.property('code').to.equal(400)
                    expect(res).to.have.property('status').to.equal(false)
                    done()
                }).catch((err) => {
                    done(err)
                })
            })

            it('Empty', (done) => {
                var newLane = {
                    th: 0,
                    width: 3.15,
                    capacity: 123,
                    path_id: pathId
                }

                var prop = ['th', 'width', 'capacity', 'path_id']
                var propIdx = Math.floor(Math.random() * 4)

                newLane[prop[propIdx]] = ''

                LaneController.addLaneOff(newLane, null).then((res) => {
                    expect(res).to.have.property('code').to.equal(400)
                    expect(res).to.have.property('status').to.equal(false)
                    done()
                }).catch((err) => {
                    done(err)
                })
            })

            describe('All Lane Has Been Registered', () => {
                before(async () => {
                    var newLanes = [
                        {
                            lane_th: 0,
                            lane_width: 3.15,
                            lane_cap: 123,
                            lane_path_id: pathId
                        },
                        {
                            lane_th: 1,
                            lane_width: 3.15,
                            lane_cap: 123,
                            lane_path_id: pathId
                        }
                    ]

                    await Lane.bulkCreate(newLanes)
                })

                after(() => {
                    Lane.destroy({ where: { lane_path_id: pathId } })
                })

                it('Test', (done) => {
                    var newLane = {
                        th: 2,
                        width: 3.15,
                        capacity: 123,
                        path_id: pathId
                    }

                    LaneController.addLaneOff(newLane, null).then((res) => {
                        expect(res).to.have.property('code').to.equal(403)
                        expect(res).to.have.property('status').to.equal(false)
                        done()
                    }).catch((err) => {
                        done(err)
                    })
                })
            })

            describe('Has Same Th', () => {
                before(async () => {
                    var newLanes = [
                        {
                            lane_th: 0,
                            lane_width: 3.15,
                            lane_cap: 123,
                            lane_path_id: pathId
                        }
                    ]

                    await Lane.bulkCreate(newLanes)
                })

                after(() => {
                    Lane.destroy({ where: { lane_path_id: pathId } })
                })

                it('Test', (done) => {
                    var newLane = {
                        th: 0,
                        width: 3.15,
                        capacity: 123,
                        path_id: pathId
                    }

                    LaneController.addLaneOff(newLane, null).then((res) => {
                        expect(res).to.have.property('code').to.equal(403)
                        expect(res).to.have.property('status').to.equal(false)
                        done()
                    }).catch((err) => {
                        done(err)
                    })
                })
            })
        })
    })
})