'use strict'

var request = require('supertest'),
    expect = require('chai').expect;
var app = require('../app');
var Admin = require('../app/models/admin');

describe('Testing Admin Controller', () => {
    describe('POST Login', () => {
        before(async () => {
            // console.log("Before")
            var newAdmin = {
                admin_username: "admin",
                admin_password: "admin",
                admin_name: "Admin",
                admin_email: "admin@cds.com",
                admin_grade: 1,
                admin_active: 0
            }

            await Admin.create(newAdmin)
        })

        after(() => {
            // console.log("After")
            console.log("POST Login after")
            Admin.destroy({ where: { admin_username: "admin" } })
        })

        it('Succeed Login', (done) => {
            var loginData = {
                username: 'super_admin',
                password: 'iamalwaysthefirst'
            }
            request(app).post('/api/admin/login').send(loginData).end((err, res) => {
                if (err) {
                    done(err)
                }
                expect(res.statusCode).to.equal(200)
                expect(res.body).to.have.property('status').to.equal(true)
                expect(res.body).to.have.property('token')
                done()
            })
        })

        it('Invalid Password', (done) => {
            var loginData = {
                username: 'super_admin',
                password: 'wrongpassword'
            }
            request(app).post('/api/admin/login').send(loginData).end((err, res) => {
                if (err) {
                    done(err)
                }
                expect(res.statusCode).to.equal(401)
                expect(res.body).to.have.property('status').to.equal(false)
                expect(res.body).to.have.property('message').to.equal('Unauthorized')
                done()
            })
        })

        it('Unable Login for unactivated admin', (done) => {
            var loginData = {
                username: 'admin',
                password: 'admin'
            }
            request(app).post('/api/admin/login').send(loginData).end((err, res) => {
                if (err) {
                    done(err)
                }
                expect(res.statusCode).to.equal(403)
                expect(res.body).to.have.property('status').to.equal(false)
                expect(res.body).to.have.property('message').to.equal('Unable to Login')
                done()
            })
        })
    })

    describe('POST Registration', () => {
        after(() => {
            console.log("POST Registration after")
            Admin.destroy({ where: { admin_username: "admin" } })
        })

        it('Succeed Registration', (done) => {
            var header = { 
                'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                'Content-Type': 'application/json'
            }

            var registrationData = {
                username: "admin",
                password: "admin",
                name: "Admin",
                email: "admin@cds.com",
                grade: 1,
                active: 0
            }

            request(app).post('/api/admin/registration').set(header).send(registrationData).end((err, res) => {
                if (err) done(err)
                expect(res.statusCode).to.equal(201)
                expect(res.body).to.have.property('status').to.equal(true)
                done()
            })
        })

        it('Same Username', (done) => {
            var header = {
                'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                'Content-Type': 'application/json'
            }

            var registrationData = {
                username: "admin",
                password: "admin",
                name: "Admin",
                email: "admin2@cds.com",
                grade: 1,
                active: 0
            }

            request(app).post('/api/admin/registration').set(header).send(registrationData).end((err, res) => {
                if (err) done(err)
                // console.log(res)
                expect(res.statusCode).to.equal(403)
                expect(res.body).to.have.property('status').to.equal(false)
                done()
            })
        })

        it('Same Email', (done) => {
            var header = {
                'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                'Content-Type': 'application/json'
            }

            var registrationData = {
                username: "admin2",
                password: "admin",
                name: "Admin",
                email: "admin@cds.com",
                grade: 1,
                active: 0
            }

            request(app).post('/api/admin/registration').set(header).send(registrationData).end((err, res) => {
                if (err) done(err)
                // console.log(res)
                expect(res.statusCode).to.equal(403)
                expect(res.body).to.have.property('status').to.equal(false)
                done()
            })
        })

        it('Missing One Data', (done) => {
            var header = {
                'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                'Content-Type': 'application/json'
            }

            var registrationData = {
                username: "admin2",
                password: "admin",
                name: "Admin",
                email: "admin2@cds.com",
                grade: 1,
                active: 0
            }

            var prop = ['username', 'password', 'name', 'email']
            var propIdx = Math.floor(Math.random() * 4)

            registrationData[prop[propIdx]] = ''
            // console.log("registrationData", registrationData)

            request(app).post('/api/admin/registration').set(header).send(registrationData).end((err, res) => {
                if (err) done(err)
                // console.log(res)
                expect(res.statusCode).to.equal(400)
                expect(res.body).to.have.property('status').to.equal(false)
                done()
            })
        })
    })

    describe('POST delete admin', () => {
        var adminId = null
        before(async () => {
            console.log('POST delete admin before')
            var newAdmin = {
                admin_username: "admin",
                admin_password: "admin",
                admin_name: "Admin",
                admin_email: "admin@cds.com",
                admin_grade: 1,
                admin_active: 0
            }

            var admin =  await Admin.create(newAdmin)
            adminId = admin.admin_id
            // console.log('POST delete admin', admin)
        })

        it('Admin data not found', (done) => {
            var header = {
                'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                'Content-Type': 'application/json'
            }

            request(app).delete('/api/admin/delete/' + 0).set(header).end((err, res) => {
                if (err) done(err)
                expect(res.statusCode).to.equal(404)
                expect(res.body).to.have.property('status').to.equal(false)
                done()
            })
        })

        it('Delete admin succeed', (done) => {
            var header = {
                'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                'Content-Type': 'application/json'
            }

            request(app).delete('/api/admin/delete/' + adminId).set(header).end((err, res) => {
                if (err) done(err)
                expect(res.statusCode).to.equal(200)
                expect(res.body).to.have.property('status').to.equal(true)
                done()
            })
        })
    })

    describe('POST Active Status', () => {
        var adminId = null
        before(async () => {
            console.log('POST delete admin before')
            var newAdmin = {
                admin_username: "admin",
                admin_password: "admin",
                admin_name: "Admin",
                admin_email: "admin@cds.com",
                admin_grade: 1,
                admin_active: 0
            }

            var admin = await Admin.create(newAdmin)
            adminId = admin.admin_id
            // console.log('POST delete admin', admin)
        })

        after(() => {
            // console.log("After")
            // console.log("POST Login after")
            Admin.destroy({ where: { admin_username: "admin" } })
        })

        it('Correct', (done) => {
            var header = {
                'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                'Content-Type': 'application/json'
            }

            request(app).post('/api/admin/status/' + true + '/' + adminId).set(header).end((err, res) => {
                if (err) done(err)
                expect(res.statusCode).to.equal(200)
                expect(res.body).to.have.property('status').to.equal(true)
                done()
            })
        })

        it('Invalid', (done) => {
            var header = {
                'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                'Content-Type': 'application/json'
            }

            request(app).post('/api/admin/status/' + true + '/' + adminId).set(header).end((err, res) => {
                if (err) done(err)
                expect(res.statusCode).to.equal(403)
                expect(res.body).to.have.property('status').to.equal(false)
                done()
            })
        })
    })

    describe('GET All Admin', () => {
        describe('Admin Data Exist', () => {
            before(async () => {
                var newAdmins = [
                    {
                        admin_username: "admin",
                        admin_password: "admin",
                        admin_name: "Admin",
                        admin_email: "admin@cds.com",
                        admin_grade: 1,
                        admin_active: 0
                    },
                    {
                        admin_username: "admin2",
                        admin_password: "admin",
                        admin_name: "Admin2",
                        admin_email: "admin2@cds.com",
                        admin_grade: 1,
                        admin_active: 0
                    },
                    {
                        admin_username: "admin3",
                        admin_password: "admin",
                        admin_name: "Admin3",
                        admin_email: "admin3@cds.com",
                        admin_grade: 1,
                        admin_active: 0
                    }
                ]

                await Admin.bulkCreate(newAdmins)
                // console.log('POST delete admin', admin)
            })

            after(() => {
                Admin.destroy({ where: { admin_grade: 1 } })
            })

            it('Check condition', (done) => {
                console.log("Check Condition")
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                request(app).get('/api/admin/all').set(header).end((err, res) => {
                    if (err) done(err)
                    expect(res.statusCode).to.equal(200)
                    expect(res.body).to.have.property('status').to.equal(true)
                    expect(res.body).to.have.property('admin_data')
                    var adminData = res.body.admin_data
                    const adminGrade = adminData.map(x => x.admin_grade);
                    expect(0).to.not.oneOf(adminGrade)
                    done()
                })
            })
        })

        describe('Admin Data Not Found', () => {
            it('Check Condition', (done) => {
                console.log("Admin Data Not Found")
                var header = {
                    'auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6NzI4OTYyOTE5OSwiZXhwIjo3Mjg5NjI5MTk5fQ.Pap1ep4NU1G7rBOtieT0w7Ys_wjTnDY6OdbNy1ZK5cI',
                    'Content-Type': 'application/json'
                }

                request(app).get('/api/admin/all').set(header).end((err, res) => {
                    if (err) done(err)
                    expect(res.statusCode).to.equal(404)
                    expect(res.body).to.have.property('status').to.equal(false)
                    expect(res.body).to.not.have.property('admin_data').with.lengthOf.at.least(3)
                    done()
                })
            })
        })
    })
})