'use strict';

var config = module.exports;

config.hmac_admin = "$B&E)H@McQfTjWnZq4t7w!z%C*F-JaNdRgUkXp2s5v8x/A?D(G+KbPeShVmYq3t6"
config.hmac_node = "MbQeThWmZq4t7w!z%C*F-J@NcRfUjXn2r5u8x/A?D(G+KbPdSgVkYp3s6v9y$B&E"

config.db = {
    user: 'root',
    password: '1234567890',
    name: 'tugas_akhir'
};

config.db.details = {
    host: '127.0.0.1',
    port: 3306,
    dialect: 'mysql',
    dialectOptions: {
        connectionTimeout: 300000,
        requestTimeout: 300000
    },
    pool: {
        max: 100,
        min: 0,
        idle: 30000
    }
}