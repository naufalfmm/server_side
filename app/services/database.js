'use strict';

var config = require('../config/index'),
    Sequelize = require('sequelize');

console.log(config)

module.exports = new Sequelize(
    config.db.name,
    config.db.user,
    config.db.password,
    config.db.details
);