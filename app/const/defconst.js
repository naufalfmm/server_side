var path = require('path');

var defaultConst = {}

defaultConst.pathUpload = "." + path.sep + "app" + path.sep + "file" + path.sep + "node"

if (process.env.NODE_ENV === 'test') {
    defaultConst.pathUpload = "." + path.sep + "test" + path.sep + "file" + path.sep + "node"
}

module.exports = defaultConst