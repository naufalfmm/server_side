'use strict'

var Sequelize = require('sequelize');
var db = require('../services/database');
var Path = require('./path');

var modelDefinition = {
    lane_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    lane_th: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    lane_width: {
        type: Sequelize.FLOAT,
        allowNull: false
    },
    lane_cap: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    // lane_path: {
    //     type: Sequelize.INTEGER,
    //     allowNull: false,
    //     validate: {
    //         min: 0,
    //         max: 1,
    //         isInt: true
    //     }
    // },
}

var laneModel = db.define('lane',modelDefinition);

Path.hasMany(laneModel, { foreignKey: { name: 'lane_path_id', allowNull: false } })
laneModel.belongsTo(Path, { foreignKey: { name: 'lane_path_id', allowNull: false } })

module.exports = laneModel