'use strict';

var Sequelize = require('sequelize');
var db = require('../services/database');
var Road = require('./road');

var modelDefinition = {
    segment_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    segment_start: {
        type: Sequelize.STRING,
        allowNull: false
    },
    segment_end: {
        type: Sequelize.STRING,
        allowNull: false
    },
    segment_paths: { // Number of Paths
        type: Sequelize.INTEGER,
        allowNull: false,
        validate: {
            min: 1,
            max: 2,
            isInt: true
        }
    },
    segment_density: {
        type: Sequelize.INTEGER,
        allowNull: false,
        validate: {
            min: 0,
            max: 4,
            isInt: true
        }
    }
}

var segmentModel = db.define('segment', modelDefinition);

Road.hasMany(segmentModel, { foreignKey: { name: 'segment_road_id', allowNull: false } })
segmentModel.belongsTo(Road, { foreignKey: { name: 'segment_road_id', allowNull: false } })

module.exports = segmentModel;