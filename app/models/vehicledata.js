'use strict';

var Sequelize = require('sequelize');
var db =  require('../services/database');

var modelDefinition = {
    vehicle_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    motorcycle_qty: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    motorcycle_meanspeed: {
        type: Sequelize.DOUBLE,
        allowNull: false
    },
    lightvehicle_qty: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    lightvehicle_meanspeed: {
        type: Sequelize.DOUBLE,
        allowNull: false
    },
    heavyvehicle_qty: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    heavyvehicle_meanspeed: {
        type: Sequelize.DOUBLE,
        allowNull: false
    },
}

var vehicledataModel = db.define("vehicledata", modelDefinition);


module.exports = vehicledataModel