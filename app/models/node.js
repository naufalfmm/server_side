'use strict';

var Sequelize = require('sequelize');
var db = require('../services/database');
var Lane = require('./lane');

var modelDefinition = {
    node_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    node_username: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false
    },
    node_key: {
        type: Sequelize.STRING,
        allowNull: false
    },
    node_config: { //Config exists or not
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
        validate: {
            min: 0,
            max: 1
        }
    }
};

var nodeModel = db.define('node', modelDefinition);

Lane.hasOne(nodeModel, {foreignKey: {name: 'node_lane_id', allowNull: false, unique: true}})
nodeModel.belongsTo(Lane, { foreignKey: { name: 'node_lane_id', allowNull: false, unique: true } })

module.exports = nodeModel;