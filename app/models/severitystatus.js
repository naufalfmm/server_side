'use strict';

var Sequelize = require('sequelize'),
    moment = require('moment');
var db = require('../services/database');
var Vehicledata = require('../models/vehicledata'),
    Node = require('../models/node');

var modelDefinition = {
    severitystatus_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    status: {
        type: Sequelize.ENUM('A','B','C','D','E','F'),
        allowNull: false,
        // get() {
        //     var tr = {"A": "Lancar", "B": "Ramai Lancar", "C": "Padat", "D": "Padat Merayap", "E": "Macet", "F": "Macet Total"}
        //     return tr[this.getDataValue('status')]
        // }
    },
    posting_time: {
        type: Sequelize.FLOAT,
        allowNull: false
    }
}

var severitystatusModel = db.define("severitystatus", modelDefinition)

Node.hasMany(severitystatusModel, {foreignKey: {name: 'severitystatus_node_id', allowNull: false}})
severitystatusModel.belongsTo(Node, { foreignKey: { name: 'severitystatus_node_id', allowNull: false } })

Vehicledata.hasOne(severitystatusModel, { foreignKey: { name: 'severitystatus_vehicledata_id', unique: true, allowNull: false } })
severitystatusModel.belongsTo(Vehicledata, {foreignKey: {name: 'severitystatus_vehicledata_id', unique: true, allowNull: false}})

module.exports = severitystatusModel