'use strict'

var Sequelize = require('sequelize'),
    bcrypt = require('bcrypt');
var db = require('../services/database');

var modelDefinition = {
    admin_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    admin_name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    admin_email: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
        validate: {
            isEmail: true
        }
    },
    admin_username: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false
    },
    admin_password: {
        type: Sequelize.STRING,
        allowNull: false
    },
    // 0 for super_admin, 1 for master_admin, 2 for admin
    admin_grade: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    admin_active: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
        validate: {
            min: 0,
            max: 1
        }
    }
}

var adminModel = db.define("admin", modelDefinition)

// adminModel.prototype.comparePass = (password)  => {
//     console.log("comparePass")
//     try {
//         bcrypt.compare(password, this.admin_password, function (isMatch) {
//             console.log(password, this.admin_password, isMatch)
//             if (isMatch) {
//                 return {status: true, code: 202, message: "Password Match"}
//             } else {
//                 return {status: false, code: 401, message: "Wrong Password"}
//             }
//         });   
//     } catch (e) {
//         return {status: false, code: 500, message: "Internal Server Error"}
//     }
// }

adminModel.addHook('afterSync', 'addAdmin', async (opt) => {
    // console.log("afterSync")
    var c = await adminModel.count()
    if (c === 0) {
        var newAdmin = {
            admin_username: 'super_admin',
            admin_password: 'iamalwaysthefirst',
            admin_name: "Super Admin",
            admin_email: "superadmin@cds.com",
            admin_grade: 0,
            admin_active: true
        }
        await adminModel.create(newAdmin)
    }
})

adminModel.addHook('beforeValidate','hashPassword', (admin) => {
    // console.log("beforeValidate", admin.changed('admin_password'))
    if (admin.changed('admin_password')) {
        return bcrypt.hash(admin.admin_password,10).then(function(pass) {
            admin.admin_password = pass
        }).catch(function(e) {
            throw new Error()
        })
    }
})

function hashPassword(admin) {
    if (admin.changed('admin_password')) {
        return bcrypt.hash(admin.admin_password, 10).then(function (password) {
            admin.admin_password = password;
        });
    }
}


module.exports = adminModel