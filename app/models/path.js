'use strict';

var Sequelize = require('sequelize');
var db = require('../services/database');
var Segment = require('./segment');

var modelDefinition = {
    path_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    path_th: {
        type: Sequelize.INTEGER,
        allowNull: false,
        validate: {
            min: 0,
            max: 1,
            isInt: true
        }
    },
    path_lanes: { // Number of Lanes
        type: Sequelize.INTEGER,
        allowNull: false,
        validate: {
            min: 1,
            isInt: true
        }
    }
}

var pathModel = db.define('path', modelDefinition);

// pathModel.addHook('afterDestroy', async (path, opt) => {
//     var segment = await Segment.findOne()
// })

Segment.hasMany(pathModel, { foreignKey: { name: 'path_segment_id', allowNull: false } })
pathModel.belongsTo(Segment, { foreignKey: { name: 'path_segment_id', allowNull: false } })

// pathModel.addHook('beforeCreate', async (path, opt) => {
//     console.log('hook beforeCreate')
//     var segments = await Segment.findOne({ where: { segment_id: path.path_segment_id }, include: [{ model: pathModel }] })

//     console.log('hook beforeCreate', segments)

//     if (segments.segment_paths <= segments.paths.length) {
//         throw new Error('No New Paths Can Be Added')
//     }

//     if (segments.paths.length > 0) {
//         var ths = segments.paths.map(p => p.path_th)
//         if (path.path_th === 1) {
//             if (ths.indexOf(path.path_th) > -1 && ths.indexOf(0) > -1) {
//                 throw new Error('No New Path Can Be Added')
//             }
//         } else {
//             if (ths.indexOf(path.path_th) > -1) {
//                 throw new Error('No New Path Can Be Added')
//             }
//         }
//     }   
// })

// pathModel.addHook('beforeBulkUpdate', async (path, opt) => {
//     var op = Sequelize.Op
//     var pathId = path.where.path_id
//     var pathData = await pathModel.findOne({ where: { path_id: pathId } })
//     var segments = await Segment.findOne({ where: { segment_id: pathData.path_segment_id }, include: [{ model: pathModel }] })

//     console.log('hook beforeBulkUpdate', path.attributes, pathData.path_th)
//     path = path.attributes

//     if (segments.segment_paths <= segments.paths.length) {
//         throw new Error('No New Paths Can Be Added')
//     }

//     if (segments.paths.length > 0) {
//         var ths = segments.paths.map(p => (p.path_id !== pathId ? p.path_th : null))
//         if (path.path_th === 1) {
//             if (ths.indexOf(path.path_th) > -1 || ths.indexOf(0) > -1) {
//                 throw new Error('No New Path Can Be Added')
//             }
//         } else {
//             if (ths.indexOf(path.path_th) > -1) {
//                 throw new Error('No New Path Can Be Added')
//             }
//         }
//     }
// })

module.exports = pathModel