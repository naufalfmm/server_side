'use strict';

var Sequelize = require('sequelize');
var db = require('../services/database');

var modelDefinition = {
    road_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    road_name: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    road_population_range: {
        type: Sequelize.INTEGER,
        allowNull: false,
        validate: {
            min: 0,
            max: 4,
            isInt: true
        }
    },
    // road_density: {
    //     type: Sequelize.INTEGER,
    //     allowNull: false,
    //     validate: {
    //         min: 0,
    //         max: 4,
    //         isInt: true
    //     }
    // }
    // road_paths: { //Count of Paths
    //     type: Sequelize.INTEGER,
    //     allowNull: false,
    //     validate: {
    //         min: 1,
    //         max: 2,
    //         isInt: true
    //     },
    // },
    // road_lanes: { //Count of Lanes
    //     type: Sequelize.INTEGER,
    //     allowNull: false,
    //     validate: {
    //         min: 1,
    //         isInt: true
    //     }
    // },
};

var roadModel = db.define('road', modelDefinition);

module.exports = roadModel;