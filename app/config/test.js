'use strict';

var config = module.exports;

config.hmac_admin = "7w9z$C&F)J@NcRfUjXn2r5u8x/A%D*G-KaPdSgVkYp3s6v9y$B&E(H+MbQeThWmZ"
config.hmac_node = "q3t6w9y$B&E)H@McQfTjWnZr4u7x!A%C*F-JaNdRgUkXp2s5v8y/B?E(G+KbPeSh"

config.env = 'test'

config.db = {
    user: 'root',
    password: '1234567890',
    name: 'cds_test'
};

config.db.details = {
    host: '127.0.0.1',
    port: 3306,
    dialect: 'mysql',
    // logging: false,
    dialectOptions: {
        connectionTimeout: 300000,
        requestTimeout: 300000
    },
    pool: {
        max: 100,
        min: 0,
        idle: 30000
    }
}