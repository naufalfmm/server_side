'use strict'

var Sequelize = require('sequelize');
var db = require('../services/database');
var Lane = require('../models/lane'),
    Path = require('../models/path'),
    Road = require('../models/road');
var RoadController = require('./roadController'),
    AuthController = require('./authController');

var laneController = {}

/*
Offline version (not able to used in API)
Parameter:
- data: {
    th:,
    width:,
    capacity:,
    path_id:
  }
- transaction: sql transaction (optional)
*/
laneController.addLaneOff = async (data, transaction) => {
    console.log("addLaneOff", data)
    console.log(1)
    try {
        await Lane.sync()
    } catch (e) {
        return ({ code: 500, status: false, message: "Internal Server Error" })
    }

    var path_id = data.path_id;

    console.log(data)

    if (data.th === undefined || data.width === undefined || data.capacity === undefined || data.path_id === undefined) {
        return { code: 400, status: false, message: "Data Incomplete" }
    }

    for (let key in data) {
        if (data[key] === '')
            return { code: 400, status: false, message: "Data Incomplete" }
    }

    // var road_data = await RoadController.getRoadOff(path_id);
    // if (!road_data.status) {
    //     return ({ code: road_data.code, status: road_data.status, message: road_data.message })
    // }
    console.log(3)
    // var laneData = await Lane.findAndCount({where: {lane_path_id: path_id}}, {include: [{model: Road, attributes: ['road_paths','road_lanes']}]})
    // var lane_count = await Lane.count({ where: { lane_path_id: path_id, lane_path: data.path } })
    // console.log(laneData)
    // var road_paths = road_data.data.road_paths,
    //     road_lanes = road_data.data.road_lanes;

    // console.log(data.th, road_lanes, lane_count, data.path, road_paths)
    // console.log(data.path < road_paths, lane_count < road_lanes, (data.th % road_lanes) <= road_lanes)
    // console.log(data.path < road_paths && lane_count < road_lanes && (data.th % road_lanes) <= road_lanes)

    // if ((data.th % road_lanes) >= lane_count || data.path >= road_paths) {
    //     return res.status(400).json({ status: false, message: "Bad Request" })
    // }
    // if (!(data.path < road_paths && lane_count < road_lanes && (data.th % road_lanes) <= road_lanes)) {
    //     console.log("Bad coy")
    //     return ({ code: 400, status: false, message: "Bad Request" })
    // }
    var op = Sequelize.Op
    var pathData = null
    var laneExist = null
    try {
        pathData = await Path.findOne({ where: { path_id: path_id }, include: [{ model: Lane }] })
        laneExist = await Lane.count({ where: { [op.and]: [{ lane_th: data.th }, { lane_path_id: path_id }] } })
    } catch (e) {
        console.log(e)
        return ({ code: 500, status: false, message: "Data Incomplete" })
    }

    console.log(pathData.path_lanes, laneExist)

    if (pathData.path_lanes <= pathData.lanes.length || laneExist > 0 || data.th >= pathData.path_lanes || data.th < 0) {
        console.log("Bad coy")
        return ({ code: 403, status: false, message: "Bad Request" })
    }

    console.log(4)
    var laneData = null
    try {
        var newLane = {
            lane_th: data.th,
            lane_width: data.width,
            lane_cap: data.capacity,
            lane_path_id: path_id
        }
        if (transaction !== null) laneData = await Lane.create(newLane, { transaction: transaction })
        else laneData = await Lane.create(newLane)
    } catch (e) {
        console.log(e)
        return ({ code: 500, status: false, message: "Internal Server Error" })
    }
    console.log(5)
    return ({ status: true, laneId: laneData.dataValues.lane_id })
}

/*
link: api/lane/add
json:
{
    th:,
    path:,
    width:,
    capacity:,
    road_id:
}
*/
laneController.addLane = async (req, res) => {
    console.log("addLane")
    var auth = req.headers.auth
    var ret = await AuthController.checkAuthentication(auth, 1)
    // console.log(1)
    if (!ret.status) {
        return res.status(ret.code).json({ status: ret.status, message: ret.message })
    }
    console.log(1)
    try {
        await Lane.sync()
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    var road_id = req.body.road_id;

    console.log(req.body)
    
    if (req.body.th === undefined || req.body.path === undefined || road_id === undefined || 
        req.body.width === undefined || req.body.capacity === undefined) {
        return res.status(400).json({ status: false, message: "Data Incomplete" })
    }
    console.log(2)
    for (let key in req.body) {
        if (req.body[key] === '')
            return res.status(400).json({ status: false, message: "Data Incomplete" })
    }

    var road_data = await RoadController.getRoadOff(road_id);
    if (!road_data.status) {
        return res.status(road_data.code).status({ status: road_data.status, message: road_data.message })
    }
    console.log(3)
    // var laneData = await Lane.findAndCount({where: {lane_road_id: road_id}}, {include: [{model: Road, attributes: ['road_paths','road_lanes']}]})
    var lane_count = await Lane.count({where: {lane_road_id: road_id, lane_path: req.body.path}})
    // console.log(laneData)
    var road_paths = road_data.data.road_paths,
        road_lanes = road_data.data.road_lanes;

    console.log(req.body.th, road_lanes, lane_count, req.body.path, road_paths)
    console.log(req.body.path < road_paths, lane_count < road_lanes, (req.body.th % road_lanes) <= road_lanes)
    console.log(req.body.path < road_paths && lane_count < road_lanes && (req.body.th % road_lanes) <= road_lanes)
    
    // if ((req.body.th % road_lanes) >= lane_count || req.body.path >= road_paths) {
    //     return res.status(400).json({ status: false, message: "Bad Request" })
    // }
    if (!(req.body.path < road_paths && lane_count < road_lanes && (req.body.th % road_lanes) <= road_lanes)) {
        console.log("Bad coy")
        return res.status(400).json({ status: false, message: "Bad Request" })
    }
    console.log(4)
    try {
        var newLane = {
            lane_th: req.body.th,
            lane_path: req.body.path,
            lane_width: req.body.width,
            lane_cap: req.body.capacity,
            lane_road_id: road_id
        }
        var lane_data = await Lane.create(newLane)
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }
    console.log(5)
    return res.status(201).json({ status: true, message: "Lane Successfully Added", lane_id: lane_data.lane_id })
}

module.exports = laneController

// laneController.getLaneOff = async (laneId) => {
//     try {
//         await Lane.sync({hooks: true})
//     } catch (e) {
//         return { status: false, code: 500, message: "Internal Server Error" }
//     }

//     try {
//         var lane = await Lane.findOne({ where: { lane_id: laneId } })

//         if (!lane.dataValues) {
//             return {status: false, code: 404, message: "Lane Not Found"}
//         }
//     } catch (e) {
//         return { status: false, code: 500, message: "Internal Server Error" }
//     }

//     return { status: true, code: 200, message: lane.dataValues }
// }