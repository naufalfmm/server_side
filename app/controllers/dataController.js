'use strict';

var Sequelize = require('sequelize');
var db = require('../services/database'),
    encrypt = require('../lib/encrypt');
var Vehicledata = require('../models/vehicledata'),
    Severitystatus = require('../models/severitystatus'),
    Node = require('../models/node'),
    Lane = require('../models/lane'),
    Path = require('../models/path'),
    Segment = require('../models/segment'),
    Road = require('../models/road');
var authController = require('./authController'),
    nodeController = require('./nodeController');

var DataController = {}

/*
link: api/data/post
json: {
    data: [
        {
            data:,
            check:
        },
        {
            data:,
            check:
        },
        ...
    ]
}
*/
DataController.postData = async (req,res) => {
    var token = req.headers.auth
    console.log("DataController postData", token, req.headers, req.body.data)
    var result = await authController.checkAuthentication(token, 3)
    if (!result.status) {
        return res.status(401).json({status: result.status, message: result.message})
    }

    try {
        await db.sync({ hooks: true })
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }
    console.log("message:", result.message)
    var node_data
    try {
        var node = await Node.findOne({where: {node_username: result.username}})
        if (node === null) {
            return res.status(403).json({ status: false, message: "Node Doesn't Exists" })
        }
        console.log("DataController postData node", node)
        node_data = node.dataValues
        console.log(node_data.node_key, node_data.node_key.length)
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    // var node_id = await nodeController.getId(result.message.username)

    // if (!node_id.status) {
    //     return res.status(400).json({status: false, message: "Unauthorizrd"})
    // }

    console.log("DataController postData node_data", node_data)

    try {
        var transPost = await db.transaction()
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    console.log("DataController postData req.data, node_data.node_key", req.body.data, node_data.node_key)

    var arrayData = req.body.data

    for (var i=0; i<arrayData.length; i++) {
        var cipher = arrayData[i].data
        var check = arrayData[i].check
        console.log("DataController check", check)

        var message = await encrypt.decryption(cipher, node_data.node_key)
        console.log("DataController message", message)
        var result_check = await encrypt.checkJSON(message, check)
        console.log("DataController result_check", result_check)

        if (!result_check.status) {
            await transPost.rollback()
            return res.status(result_check.code).json({ status: false, message: result_check.message })
        }

        var json_data = result_check.message
        console.log(json_data)

        try {
            var newVehicledata = {
                motorcycle_qty: json_data.mtcqty,
                motorcycle_meanspeed: json_data.mtcms,
                lightvehicle_qty: json_data.lvqty,
                lightvehicle_meanspeed: json_data.lvms,
                heavyvehicle_qty: json_data.hvqty,
                heavyvehicle_meanspeed: json_data.hvms
            }

            var vehicledata = await Vehicledata.create(newVehicledata, { transaction: transPost })
            var vehicledata_id = vehicledata.dataValues.vehicle_id
            // console.log(vehicledata_id)
        } catch (e) {
            // console.log(e)
            await transPost.rollback()
            return res.status(500).json({ status: false, message: "Internal Server Error" })
        }

        try {
            var newSeveritystatus = {
                status: json_data.status,
                severitystatus_node_id: node_data.node_id,
                severitystatus_vehicledata_id: vehicledata_id,
                posting_time: json_data.time
            }
            console.log(newSeveritystatus)

            await Severitystatus.create(newSeveritystatus, { transaction: transPost })
            // var severitystatus_id = severitystatus.dataValues.severitystatus_id
        } catch (e) {
            // console.log(e)
            await transPost.rollback()
            return res.status(500).json({ status: false, message: "Internal Server Error" })
        }
    }

    await transPost.commit()
    return res.status(201).json({ status: true, message: "Data is Successfully Added" })
}

/*
link: api/data/:sevStatus
*/
DataController.getData = async (req, res) => {
    var token = req.headers.auth

    console.log("DataController getData", req.params.sevStatus)
    console.log(token, req.headers)
    var result = await authController.checkAuthentication(token, 2)
    if (!result.status) {
        return res.status(401).json({ status: result.status, message: result.message })
    }

    try {
        await db.sync({ hooks: true })
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    var sevStat = req.params.sevStatus
    if (sevStat === undefined) {
        return res.status(400).json({ status: false, message: "Data Incomplete" })
    }

    var data = null
    try {
        // data = await Road.findAll({
        //     include: [{
        //         model: Segment, include: [{
        //             model: Path, include:
        //                 [{
        //                     model: Lane, include: [{
        //                         model: Node, attributes: ['node_id', 'node_username'], include:
        //                             [{ model: Severitystatus, where: {status: sevStat}, include: [{ model: Vehicledata }] }]
        //                     }]
        //                 }]
        //         }]
        //     }]
        // })
        data = await Severitystatus.findAll({where: {status: sevStat}, include: [{model: Vehicledata}, {model: Node, include: [{model: Lane, include: [{model: Path, include: [{model: Segment, include: [{model: Road}]}]}]}]}]})
        if (data.length === 0) {
            return res.status(404).json({ status: false, message: "Severity Data Not Found" })
        }
    } catch (e) {
        console.log(e)
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    try {
        console.log(data)
    } catch (e) {
        return res.status(500).json({ status: false, message: e.message })
    }

    return res.status(200).json({ status: true, data: data })
}

/*
link: api/data/:dataId
*/
DataController.getDataId = async (req, res) => {
    var token = req.headers.auth

    console.log("DataController getDataId", req.params.dataId)
    var result = await authController.checkAuthentication(token, 2)
    if (!result.status) {
        return res.status(401).json({ status: result.status, message: result.message })
    }
    
    if (req.params.dataId === undefined) {
        return res.status(400).json({ status: false, message: "Data Incomplete" })
    }

    console.log(0)
    try {
        await db.sync({ hooks: true })
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }
    console.log(1)
    var data = null
    try {
        data = await Severitystatus.findOne({ where: { severitystatus_id: req.params.dataId }, include: 
            [{ model: Vehicledata }, { model: Node, include: [{ model: Lane, include: 
            [{ model: Path, include: [{ model: Segment }] }] }] }] })
        
        if (data === null) {
            return res.status(404).json({ status: false, message: "Severity Data Not Found" })
        }
    } catch (e) {
        console.log(e)
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }
    console.log(2)
    return res.status(200).json({ status: true, data: data })
}

/*
link: api/data/severity
*/
DataController.getSeverityData = async (req, res) => {
    var token = req.headers.auth
    console.log("getSeverityData", token)
    var result = await authController.checkAuthentication(token, 2)

    if (!result.status) {
        return res.status(401).json({ status: result.status, message: result.message })
    }

    console.log("DataController getSeverityData")

    try {
        await db.sync({ hooks: true })
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    var data = null
    try {
        data = await Severitystatus.findAll({include: [{model: Vehicledata}]})
        console.log(data)
    } catch (e) {
        console.log(e)
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    try {
        console.log(data)
    } catch (e) {
        return res.status(500).json({ status: false, message: e.message })
    }

    return res.status(200).json({ status: true, data: data })
}

/*
link: api/data/all
*/
DataController.getDataAll = async (req, res) => {
    var token = req.headers.auth
    var result = await authController.checkAuthentication(token, 2)

    if (!result.status) {
        return res.status(401).json({ status: result.status, message: result.message })
    }

    try {
        await db.sync({ hooks: true })
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    var data = null
    try {
        data = await Road.findAll({ include: [{ model: Segment, include: [{ model: Path, include: 
            [{ model: Lane, include: [{ model: Node, attributes: ['node_id', 'node_username'], include: 
            [{ model: Severitystatus, include: [{ model: Vehicledata }] }] }] }] }] }] })

        // data = await Severitystatus.findAll({include: [{model: Vehicledata}]})
        console.log(data)
    } catch (e) {
        console.log(e)
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    try {
        console.log(data)
    } catch (e) {
        return res.status(500).json({ status: false, message: e.message })
    }

    return res.status(200).json({ status: true, data: data })
}

module.exports = DataController