'use strict';

var Sequelize = require('sequelize'),
    db = require('../services/database');
var Road = require('../models/road'),
    Segment = require('../models/segment'),
    Path = require('../models/path'),
    Lane = require('../models/lane'),
    Node = require('../models/node');
var AuthController = require('./authController'),
    SegmentController = require('./segmentController');

var roadController = {}

roadController.getRoadOff = async (road_id) => {
    try {
        await Road.sync()
    } catch (e) {
        return {code: 500, status: false, message: "Internal Server Error"}
    }

    var road = await Road.findOne({where: {road_id: road_id}})
    if (!road) {
        return {code: 404, status: false, message: "Road Not Found"}
    }

    return {status: true, data: road.dataValues}
}

/*
link: api/road/add
json: {
    name:,
    pop_range:,
    segment: [] (optional)
}
*/
roadController.addRoad = async (req, res) => {
    var auth = req.headers.auth
    var ret = await AuthController.checkAuthentication(auth, 1)

    if (!ret.status) {
        return res.status(ret.code).json({ status: ret.status, message: ret.message })
    }

    try {
        await Road.sync()
    } catch (e) {
        return res.status(500).json({status: false, message: "Internal Server Error"})
    }

    if (req.body.name === undefined || req.body.pop_range === undefined) {
        return res.status(400).json({status: false, message: "Data Incomplete"})
    }

    for (let key in req.body) {
        if (req.body[key] === '') 
            return res.status(400).json({ status: false, message: "Data Incomplete" })
    }

    var transRoad = null
    var segment = null
    if (req.body.segment !== undefined) {
        segment = req.body.segment.slice()
        if (segment.length > 0) {
            try {
                transRoad = await db.transaction()   
            } catch (e) {
                await transRoad.rollback()
                return res.status(500).json({ status: false, message: "Internal Server Error" })
            }
        }
    }

    var roadData = null
    try {
        console.log("post", req.body)
        var newRoad = {
            road_name: req.body.name,
            road_population_range: req.body.pop_range
        }

        if (transRoad !== null) roadData = await Road.create(newRoad, {transaction: transRoad})
        else roadData = await Road.create(newRoad)
    } catch (e) {
        await transRoad.rollback()
        return res.status(500).json({status: false, message: e.message})
    }
    var roadId = roadData.dataValues.road_id
    console.log(0)
    if (segment !== null) {
        if (segment.length > 0) {
            for (var i = 0; i < segment.length; i++) {
                var addSegment = { ...segment[i], roadId: roadId }
                var dataSegment = await SegmentController.addSegmentOff(addSegment, transRoad)
                if (!dataSegment.status) {
                    await transRoad.rollback()
                    return res.status(dataSegment.code).json({ status: dataSegment.status, message: dataSegment.message })
                }
            }
        }
    }

    console.log(1)
    if (transRoad !== null) await transRoad.commit()
    return res.status(201).json({status: true, message: "Road Successfully Added"})
}

/*
link: api/road/edit/:roadId
json: {
    name:,
    pop_range:
}
*/
roadController.editRoad = async (req, res) => {
    var auth = req.headers.auth
    var ret = await AuthController.checkAuthentication(auth, 1)

    if (!ret.status) {
        return res.status(ret.code).json({ status: ret.status, message: ret.message })
    }

    var road_id = req.params.roadId
    if (road_id === null) {
        return res.status(400).json({ status: false, message: "Data Incomplete" })
    }

    var op = Sequelize.Op

    if (req.body.name === undefined || req.body.pop_range === undefined) {
        return res.status(400).json({ status: false, message: "Data Incomplete" })
    }

    for (let key in req.body) {
        if (req.body[key] === '')
            return res.status(400).json({ status: false, message: "Data Incomplete" })
    }

    // if (req.body.name.length > 0 || (req.body.paths >= 1 && req.body.paths <= 2) || req.body.lanes >= 1 ||
    //     (req.body.pop_range >= 0 && req.body.pop_range <= 4) || (req.body.density >= 0 && req.body.density <= 4)) {
    //     return res.status(400).json({ status: false, message: "Data Invalid" })
    // }

    try {
        await Road.sync()
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    // var road_get = null
    // try {
    //     road_get = await Road.findOne({ where: { road_id: road_id } })
    // } catch (e) {
    //     return res.status(500).json({ status: false, message: "Internal Server Error" })
    // }
    // const road_data = road_get.dataValues
    // console.log(road_data)

    // console.log(road_data)

    // if (req.body.lanes < road_data.road_lanes) {
    //     const lane_count = road_data.lanes.length
    //     if (lane_count > req.body.lanes) {
    //         return res.status(403).json({ status: false, mesage: "Lanes Cannot be Edited" })
    //     }
    // }

    // if (req.body.paths < road_data.road_paths && road_data.lanes.length > 0) {
    //     const used_path = road_data.lanes.slice().map(l => l.lane_path).sort()
    //     if (used_path[used_path.length - 1] + 1 > req.body.paths) {
    //         return res.status(403).json({ status: false, mesage: "Directions Cannot be Edited" })
    //     }
    // } else {
    //     const lane_count = road_data.lanes.length
    //     console.log(lane_count, road_data.road_lanes)
    //     if (lane_count === road_data.road_lanes && req.body.lanes <= road_data.road_lanes) {
    //         return res.status(403).json({ status: false, mesage: "Directions Cannot be Edited" })
    //     }
    // }

    try {
        var updateRoad = {
            road_name: req.body.name,
            road_population_range: req.body.pop_range
        }

        var road_count = await Road.update(updateRoad, { where: { road_id: road_id } })
    } catch (e) {
        return res.status(500).json({ status: false, message: e.message })
    }

    return res.status(201).json({ status: true, message: "Road Successfully Edited" })
}

/*
link: api/road/delete/:roadId
*/
roadController.deleteRoad = async (req, res) => {
    console.log('deleteRoad')
    var auth = req.headers.auth
    var ret = await AuthController.checkAuthentication(auth, 1)

    if (!ret.status) {
        return res.status(ret.code).json({ status: ret.status, message: ret.message })
    }

    var road_id = req.params.roadId
    if (road_id === null) {
        return res.status(400).json({ status: false, message: "Data Incomplete" })
    }

    try {
        await Road.sync()
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    try {
        var row_deleted = await Road.destroy({ where: { road_id: road_id } })
        if (row_deleted === 0) {
            return res.status(403).json({ status: false, message: "Road Doesn't Exist" })
        }
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    return res.status(200).json({ status: true, message: "Road is Successfully Deleted" })
}

/*
link: api/road/:roadId
*/
roadController.getRoad = async (req, res) => {
    var auth = req.headers.auth
    var ret = await AuthController.checkAuthentication(auth, 2)

    if (!ret.status) {
        return res.status(ret.code).json({ status: ret.status, message: ret.message })
    }

    var road_id = req.params.roadId
    if (road_id === null) {
        return res.status(400).json({ status: false, message: "Data Incomplete" })
    }

    try {
        await Road.sync()
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    try {
        var roadData = await Road.findOne({where: {road_id: road_id}, include:[{model: Segment,
            include: [{model: Path, include: [{model: Lane, include: [{model: Node}]}]}]}]})
        
        if (roadData === null) {
            return res.status(404).json({ status: false, message: "Road Data Not Found" })
        }
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    return res.status(200).json({ status: true, data: roadData })
}

/*
link: api/road/all
*/
roadController.getRoadAll = async (req, res) => {
    var auth = req.headers.auth
    var ret = await AuthController.checkAuthentication(auth, 2)

    if (!ret.status) {
        return res.status(ret.code).json({ status: ret.status, message: ret.message })
    }

    try {
        await Road.sync()
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    try {
        var roadDataAll = await Road.findAll({
                attributes: ['road_id', 'road_name', 'road_population_range']
            })
        
        if (roadDataAll.length === 0) {
            return res.status(404).json({ status: false, message: "Road Doesn't Exist" })
        }
    } catch (e) {
        console.log(e)
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    try {
        var road_full = await Road.findAll({ include: [{ model: Segment, include: [{ model: Path, 
            include: [{ model: Lane, include: [{ model: Node }] }] }] }] })
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    return res.status(200).json({ status: true, road_data: roadDataAll, road_full: road_full })
}

module.exports = roadController