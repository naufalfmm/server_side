'use strict';

var Sequelize = require('sequelize'),
    jwt = require('jsonwebtoken'),
    bcrypt = require('bcrypt');
var config = require('../config/index'),
    db = require('../services/database');
var Admin = require('../models/admin');
var AuthController = require('./authController');

var adminController = {}

/*
link: api/admin/registration
json: {
    username:,
    password:,
    name:,
    email:,
    grade:,
    active:
}
*/
adminController.registration = async (req, res) => {
    var auth = req.headers.auth
    var ret = await AuthController.checkAuthentication(auth, 1)

    if (!ret.status) {
        return res.status(ret.code).json({ status: ret.status, message: ret.message })
    }

    if (!req.body.username || !req.body.password || !req.body.name || !req.body.email || req.body.active === undefined || req.body.grade === undefined) {
        return res.status(400).json({ status: false, message: "Bad Request" })
    }

    if (req.body.grade < 1 || req.body.grade > 2) {
        return res.status(400).json({ status: false, message: "Bad Request" })
    }

    try {
        await Admin.sync()
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    if (ret.adminData.admin_grade === 1) {
        if (req.body.grade === 0 || req.body.grade === 1) {
            return res.status(403).json({ status: false, message: "Super Admin Can't Be Added" })
        }
    }

    try {
        var newAdmin = {
            admin_username: req.body.username,
            admin_password: req.body.password,
            admin_name: req.body.name,
            admin_email: req.body.email,
            admin_grade: req.body.grade,
            admin_active: req.body.active
        }
        
        await Admin.create(newAdmin)
    } catch (e) {
        return res.status(403).json({ status: false, message: e.message })
    }

    var gradeMsg = "Master Admin"
    if (req.body.grade === 2) {
        gradeMsg = "Admin"
    }

    return res.status(201).json({ status: true, message: gradeMsg + " is Successfully Registered" })
}

/*
link: api/admin/edit/:adminId
json: {
    username:,
    name:,
    email:
}
*/

// adminController.editAdminData = async (req, res) => {
//     var auth = req.headers.auth
//     var ret = await AuthController.checkAuthentication(auth, 1)

//     if (!ret.status) {
//         return res.status(ret.code).json({ status: ret.status, message: ret.message })
//     }

//     var admin_id = req.params.adminId
//     if (admin_id === null) {
//         return res.status(400).json({ status: false, message: "Data Incomplete" })
//     }

//     try {
//         await Admin.sync()
//     } catch (e) {
//         return res.status(500).json({ status: false, message: "Internal Server Error" })
//     }
// }


/*
JUST FOR SUPER ADMIN (0) & MASTER ADMIN (1)
link: api/admin/delete/:adminId
*/
adminController.deleteAdmin = async (req, res) => {
    var auth = req.headers.auth
    var ret = await AuthController.checkAuthentication(auth, 1)

    if (!ret.status) {
        return res.status(ret.code).json({ status: ret.status, message: ret.message })
    }

    var admin_id = req.params.adminId
    if (admin_id === null) {
        return res.status(400).json({ status: false, message: "Data Incomplete" })
    }

    try {
        await Admin.sync()
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    try {
        var admin_data = await Admin.findOne({ where: { admin_id: admin_id } })

        console.log("adminController delete admin", admin_data)

        if (admin_data === null) {
            return res.status(404).json({ status: false, message: "Admin Data Not Found" })
        }
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    if (admin_data.dataValues.admin_grade === 0) {
        return res.status(403).json({ status: false, message: "Super Admin Can't be Deleted" })
    } else if (admin_data.dataValues.admin_grade === 1) {
        if (ret.adminData.admin_grade === 2 || ret.adminData.admin_grade === 2) {
            return res.status(403).json({ status: false, message: "Master Admin Can't be Deleted" })
        }
    }

    try {
        var row_deleted = await Admin.destroy({ where: { admin_id: admin_id } })
        if (row_deleted === 0) {
            return res.status(403).json({ status: false, message: "Admin Doesn't Exist" })
        }
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    return res.status(200).json({ status: true, message: "Admin is Successfully Deleted" })
}

/*
link: api/admin/unregist/:adminId
json: {
    username:,
    password:
}
*/
adminController.unregistration = async (req, res) => {
    var auth = req.headers.auth
    var ret = await AuthController.checkAuthentication(auth, 2)

    if (!ret.status) {
        return res.status(ret.code).json({ status: ret.status, message: ret.message })
    }

    if (!req.body.username || !req.body.password) {
        return res.status(400).json({ status: false, message: "Bad Request" })
    }

    console.log(req.body.username, req.body.password)

    var admin_id = req.params.adminId
    if (admin_id === null) {
        return res.status(400).json({ status: false, message: "Data Incomplete" })
    }

    if (adminData.admin_username !== req.body.username) {
        return res.status(401).json({ status: false, message: "Unauthorized" })
    }

    try {
        await Admin.sync()
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    try {
        var admin_data = await Admin.findOne({ where: { admin_username: req.body.username } })
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    if (admin_data === null) {
        return res.status(401).json({ status: false, message: "Unauthorized" })
    }

    if (admin_data.dataValues.admin_grade === 0) {
        return res.status(403).json({ status: false, message: "Super Admin Can't be Deleted" })
    }

    bcrypt.compare(req.body.password, admin_data.dataValues.admin_password, async (err, isMatch) => {
        if (err) {
            return res.status(500).json({ status: false, message: "Internal Server Error" })
        } else {
            if (!isMatch) {
                return res.status(401).json({ status: false, message: "Unauthorized" })
            } else {
                try {
                    var row_deleted = await Admin.destroy({ where: { admin_id: admin_id } })
                    if (row_deleted === 0) {
                        return res.status(403).json({ status: false, message: "Admin Doesn't Exist" })
                    }
                } catch (e) {
                    return res.status(500).json({ status: false, message: "Internal Server Error" })
                }

                return res.status(200).json({ status: true, message: "Admin is Successfully Deleted" })
            }
        }
    })
}

/*
link: api/admin/status/:activeStatus/:adminId
*/
adminController.changeActiveStatus = async (req, res) => {
    const op = Sequelize.Op
    var auth = req.headers.auth
    var ret = await AuthController.checkAuthentication(auth, 0)

    if (!ret.status) {
        return res.status(ret.code).json({ status: ret.status, message: ret.message })
    }

    console.log('changeActiveStatus')

    try {
        await Admin.sync()
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    var status = req.params.activeStatus
    var admin_id = req.params.adminId
    if (admin_id === null || status === null) {
        return res.status(400).json({ status: false, message: "Data Incomplete" })
    }

    try {
        var row_count = await Admin.count({ where: { [op.and]: [{ admin_id: admin_id }, { admin_grade: 0 }] }})

        if (row_count > 0) {
            return res.status(403).json({ status: false, message: "Unable to Updated" })
        }
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    var statusInt = 0
    if (status === 'true') statusInt = 1

    try {
        var row_updated = await Admin.update({admin_active: statusInt}, {where: {admin_id: admin_id}})
        if (row_updated[0] === 0) {
            return res.status(403).json({ status: false, message: "Unable to Updated" })
        }
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    return res.status(200).json({ status: true, message: "Active Admin Status Updated" })
}

/*
link: api/admin/login
json: {
    username:,
    password:
}
*/
adminController.login = async (req, res) => {
    if (!req.body.username || !req.body.password) {
        return res.status(400).json({ status: false, message: "Bad Request" })
    }

    var username = req.body.username
    var password = req.body.password

    try {
        await Admin.sync()
    } catch (e) {
        return res.status(500).json({status: false, message: "Internal Server Error"})
    }
    
    try {
        var admin_data = await Admin.findOne({ where: { admin_username: username } })   
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    // console.log(admin_data)

    if (admin_data === null) {
        return res.status(401).json({status: false, message: "Unauthorized"})
    }

    if (!admin_data.dataValues.admin_active) {
        return res.status(403).json({ status: false, message: "Unable to Login" })
    }

    var statusAdmin = "admin"
    if (admin_data.dataValues.admin_grade === 0) statusAdmin = "superadmin"
    else if (admin_data.dataValues.admin_grade === 1) statusAdmin = "masteradmin"
    var hmacUsed = config.hmac_admin

    bcrypt.compare(password, admin_data.dataValues.admin_password,(err, isMatch) => {
        if (err) {
            return res.status(500).json({status: false, message: "Internal Server Error"})
        } else {
            if (!isMatch) {
                return res.status(401).json({ status: false, message: "Unauthorized" })
            } else {
                var token = jwt.sign({ username: username, status: statusAdmin }, hmacUsed, { expiresIn: '30000m' })

                return res.status(200).json({ status: true, token: token, message: "Happy Login!" })
            }
        }
    })
}

/*
link: api/admin/all
*/
adminController.getAllAdmin = async (req, res) => {
    var auth = req.headers.auth
    var ret = await AuthController.checkAuthentication(auth, 1)

    if (!ret.status) {
        return res.status(ret.code).json({ status: ret.status, message: ret.message })
    }

    try {
        await Admin.sync()
    } catch (e) {
        console.log('500 admin sync')
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    try {
        console.log(ret, ret.adminData)
        var adminGrade = [2]
        if (ret.adminData.admin_grade === 0) adminGrade = [1, 2]

        var adminDataAll = await Admin.findAll({
            attributes: ['admin_id', 'admin_name', 'admin_username', 'admin_email', 'admin_active', 'admin_grade'],
        where: {admin_grade: adminGrade}})

        if (adminDataAll.length === 0) {
            return res.status(404).json({ status: false, message: "Admin Doesn't Exist" })
        }
    } catch (e) {
        console.log('500 admin get', e)
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    return res.status(200).json({ status: true, admin_data: adminDataAll })
}

module.exports = adminController