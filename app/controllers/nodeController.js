'use strict';

var Sequelize = require('sequelize'),
    ff = require('../lib/file'),
    fs = require('fs'),
    pathFile = require('path'),
    jwt = require('jsonwebtoken'),
    KeygenAES = require('../lib/encrypt'),
    config = require('../config/index');
var db = require('../services/database');
var Node = require('../models/node'),
    Lane = require('../models/lane'),
    Path = require('../models/path'),
    Segment = require('../models/segment'),
    Road = require('../models/road');
var AuthController = require('./authController'),
    LaneController = require('./laneController');
var defConst = require('../const/defconst');

var NodeController = {};

/*
link: /api/node/login
*/
NodeController.login = async (req, res) => {
    if (!req.body.username) {
        return res.status(400).json({ status: false, message: "Bad Request" })
    }

    var username = req.body.username
    console.log(1)
    try {
        await Node.sync({ hooks: true })
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error (Sync)" })
    }
    console.log(2)
    try {
        var count = await Node.count({where: {node_username: username}})

        if (count === 0) {
            return res.status(401).json({status: false, message: "Unauthorized"})
        }
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }
    console.log(3)
    var token = jwt.sign({ username: username, status: "node" }, config.hmac_node, { expiresIn: '30000m' })
    console.log(4)
    return res.status(200).json({ status: true, token: token, message: "Happy Login!" })
}

NodeController.getId = async (username) => {
    try {
        await Node.sync()
    } catch (e) {
        return {status: false, message: "Internal Server Error"}
    }
    var node = await Node.findOne({where: {node_username: username}})
    if (!node) {
        return {status: false, message: "Node Not Found"}
    } else {
        return {status: true, message: node.id}
    }
}

/*
link: api/node/add/
json:
{
    th:,
    width:,
    capacity:,
    path_id:
}
*/
NodeController.addNode = async (req, res) => {
    console.log("addnode", req.body)
    // console.log(req.headers.auth)
    var auth = req.headers.auth
    var ret = await AuthController.checkAuthentication(auth, 1)
    console.log(1)
    if (!ret.status) {
        return res.status(ret.code).json({ status: ret.status, message: ret.message })
    }
    console.log(1.1, req.body.th, req.body.capacity, req.body.path_id, req.body.width)
    if (req.body.th === undefined || req.body.path_id === undefined ||
        req.body.width === undefined || req.body.capacity === undefined) {
        return res.status(400).json({ status: false, message: "Data Incomplete" })
    }
    console.log(1.2)
    for (let key in req.body) {
        if (req.body[key] === '')
            return res.status(400).json({ status: false, message: "Data Incomplete" })
    }
    console.log(2)
    try {
        await Node.sync()
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }
    console.log(3)
    var transNode = null
    try {
        transNode = await db.transaction()
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }
    console.log(4)    
    var statRet = await LaneController.addLaneOff({...req.body}, transNode)
    if (!statRet.status) {
        await transNode.rollback()
        return res.status(statRet.code).json({ status: statRet.status, message: statRet.message })
    }

    // console.log(req.body)

    var laneId = statRet.laneId
    // // console.log(road_id,path,lane,cap)

    // if (laneId === null) {
    //     return res.status(400).json({ status: false, message: "Data Incomplete" })
    // }

    // var next_node = await Node.count() + 1
    var key = await KeygenAES.generatekey()
    var username = "node_" + laneId.toString()
    
    // console.log(transNode)
    console.log(5)
    try {
        var newNode = {
            node_username: username,
            node_key: key,
            node_lane_id: laneId
        }

        await Node.create(newNode, {transaction: transNode})
    } catch (e) {
        await transNode.rollback()
        if (e instanceof Sequelize.UniqueConstraintError) {
            return res.status(409).json({ status: false, message: "Unique Constraint Error" })
        } else if (e instanceof Sequelize.ForeignKeyConstraintError) {
            return res.status(409).json({ status: false, message: "Lane ID Error" })
        }
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }
    console.log(6)
    await transNode.commit()
    console.log(7)
    return res.status(201).json({ status: true, message: "Node Successfully Created" })
}

/*
link: api/node/delete/:nodeId
*/
NodeController.deleteNode = async(req, res) => {
    var auth = req.headers.auth
    var ret = await AuthController.checkAuthentication(auth, 1)
    if (!ret.status) {
        return res.status(ret.code).json({ status: ret.status, message: ret.message })
    }

    var node_id = req.params.nodeId
    if (node_id === null) {
        return res.status(400).json({ status: false, message: "Data Incomplete" })
    }

    try {
        await Node.sync()
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    try {
        var nodeData = await Node.findOne({ where: { node_id: node_id } })
        if (nodeData === null) {
            return res.status(403).json({ status: false, message: "Node Doesn't Exist" })
        }
        var laneId = nodeData.dataValues.node_lane_id
        // var row_deleted = await Node.destroy({where: {node_id: node_id}}, {include: [{model: Lane}]})
        var row_deleted = await Lane.destroy({ where: { lane_id : laneId } })
        if (row_deleted === 0) {
            return res.status(403).json({ status: false, message: "Node Doesn't Exist" })
        }
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    return res.status(200).json({ status: true, message: "Node is Successfully Deleted" })
}

/*
link: api/node/all
*/

NodeController.getNodeAll = async (req, res) => {
    console.log('getNodeAll')
    var auth = req.headers.auth
    var ret = await AuthController.checkAuthentication(auth, 2)
    
    if (!ret.status) {
        return res.status(ret.code).json({ status: ret.status, message: ret.message })
    }

    try {
        await Node.sync()
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    try {
        var nodeDataAll = await Node.findAll({
            attributes: ['node_id', 'node_username', 'node_key', 'node_config'],
            include: [{ model: Lane, attributes: ['lane_th', 'lane_width', 'lane_cap']}]
        })

        if (nodeDataAll.length === 0) {
            return res.status(404).json({ status: false, message: "Node Doesn't Exist" })
        }
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    return res.status(200).json({ status: true, node_data: nodeDataAll })
}

/*
link: api/node/:nodeId
*/
NodeController.getNode = async (req, res) => {
    console.log("getNode")
    var auth = req.headers.auth
    var ret = await AuthController.checkAuthentication(auth, 2)

    if (!ret.status) {
        return res.status(ret.code).json({ status: ret.status, message: ret.message })
    }

    var node_id = req.params.nodeId
    if (node_id === null) {
        return res.status(400).json({ status: false, message: "Data Incomplete" })
    }
    console.log(1)

    try {
        await Node.sync()
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }
    console.log(2)

    var nodeDataAll = null
    var segmentData = null
    try {
        nodeDataAll = await Node.findOne({ where: { node_id: node_id }, attributes: ['node_id', 'node_username', 'node_key', 'node_config'], include: 
            [{ model: Lane, attributes: ['lane_th', 'lane_width', 'lane_cap'], include: 
            [{ model: Path, attributes: ['path_th', 'path_lanes'], include: 
            [{ model: Segment, attributes: ['segment_id', 'segment_start', 'segment_end', 'segment_paths'], include: 
            [{ model: Road, attributes: ['road_name'] }] }] }] }] })

        if (nodeDataAll === null) {
            return res.status(404).json({ status: false, message: "Node Doesn't Exist" })
        }
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    const segmentId = nodeDataAll.lane.path.segment.segment_id
    try {
        segmentData = segmentData = await Segment.findOne({ where: { segment_id: segmentId }, include: 
            [{model: Path, attributes: ['path_th','path_lanes']}]})
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    return res.status(200).json({ status: true, node_data: nodeDataAll, segment_data: segmentData })
}

/*
link: api/node/create/:nodeId
*/
NodeController.createConfig = async (req, res) => {
    console.log("createConfig start", req.params.nodeId)
    var auth = req.headers.auth
    var ret = await AuthController.checkAuthentication(auth, 1)
    // console.log(1)
    if (!ret.status) {
        return res.status(ret.code).json({ status: ret.status, message: ret.message })
    }

    var node_id = req.params.nodeId
    console.log(node_id)
    if (node_id === null) {
        return res.status(400).json({ status: false, message: "Data Incomplete" })
    }

    var node = null
    var segment = null
    try {
        node = await Node.findOne({ where: { 'node_id': node_id }, include: 
            [{ model: Lane, attributes: ['lane_width', 'lane_cap'], include: 
            [{ model: Path, attributes: ['path_lanes'], include: 
            [{ model: Segment, attributes: ['segment_paths'] }] }] }] })

        segment = await Segment.findOne({ attributes: ['segment_paths'], include: 
            [{ model: Path, attributes: ['path_lanes'], include: 
            [{ model: Lane, attributes: ['lane_width', 'lane_cap'], include: 
            [{ model: Node, where: { 'node_id': node_id } }] }] }] })
        if (node === null) {
            return res.status(403).json({ status: false, message: "Node Doesn't Exist" })
        }
        var node_data = node.dataValues
        var segment_data = segment.dataValues

        if (node_data === null) {
            return res.status(404).json({ status: false, message: "Node Not Found" })
        }
    } catch (e) {
        console.log(e)
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    var node_username = node_data.node_username,
        node_key = node_data.node_key;
    // console.log(defConst.pathUpload + path.sep + node_username)
    var status_ff = await ff.createFolder(defConst.pathUpload + pathFile.sep + node_username)
    // console.log("status_ff")
    if (!status_ff.status) {
        return res.status(status_ff.code).json({ status: false, message: status_ff.message })
    }

    var path = segment_data.segment_paths,
        lane_width = node_data.lane.lane_width,
        lane_cap = node_data.lane.lane_cap;

    var pathsData = segment_data.paths.slice()
    
    var lanes = 0
    var laneData = [0,0]
    for (var i=0; i<pathsData.length; i++) {
        lanes += pathsData[i].path_lanes
        laneData[i] = pathsData[i].path_lanes
    }

    var road_type = null
    if (lanes >= 6) {
        road_type = 2
    } else if (lanes >= 4) {
        if (path === 2) {
            road_type = 1
        } else {
            road_type = 2
        }
    } else if (lanes === 3) {
        if (path === 2) {
            road_type = 0
        } else {
            road_type = 2
        }
    } else if (lanes === 2) {
        if (path === 2) {
            road_type = 2
        } else {
            road_type = 1
        }
    } else {
        road_type = 0
    }

    console.log(pathFile.sep)

    var data = "key = \"" + node_key + "\"\n" + "username = \"" + node_username + "\"\n" + "road_type = " + road_type.toString() + "\n" + "lane_width = " + lane_width.toString() + "\n" + "lane_cap = " + lane_cap.toString() + "\n"
    console.log("writeFile", data)
    try {
        await fs.writeFileSync(defConst.pathUpload + pathFile.sep + node_username + pathFile.sep + "config.py", data)
    } catch (e) {
        console.log(e)
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    try {
        await Node.update({ 'node_config': 1 }, { where: { node_id: node_id } })
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    return res.status(201).json({ status: true, message: "Config File Created Successfully" })
}

/*
link: api/node/download/:nodeId
*/
NodeController.downloadConfig = async (req, res) => {
    var auth = req.headers.auth
    var ret = await AuthController.checkAuthentication(auth, 1)
    // console.log(1)
    if (!ret.status) {
        return res.status(ret.code).json({ status: ret.status, message: ret.message })
    }

    var node_id = req.params.nodeId
    // console.log(node_id)

    if (node_id === null) {
        return res.status(400).json({ status: false, message: "Data Incomplete" })
    }

    try {
        var node = await Node.findOne({ where: { node_id: node_id }, attributes: ['node_username'] })

        if (node === null) {
            return res.status(404).json({ status: false, message: "Node Doesn't Exist" })
        }

        var node_data = node.dataValues
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    var file_path = defConst.pathUpload + pathFile.sep + node_data.node_username + pathFile.sep + "config.py",
        filename = "config.py";
    // console.log(file_path)
    var check_file = await ff.checkFile(file_path)

    if (!check_file.status) {
        return res.status(404).json({ status: false, message: "File Not Found" })
    }

    try {
        await Node.update({ 'node_config': 1 }, { where: { node_id: node_id } })
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }
    
    return res.download(file_path,filename)
}

module.exports = NodeController;