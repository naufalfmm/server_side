'use strict';

var Sequelize = require('sequelize'),
    jwt = require('jsonwebtoken');
var config = require('../config/index');
var Node = require('../models/node'),
    Admin = require('../models/admin');
    // Coba = require('../models/coba');

var authController = {};

authController.checkAuthentication = async (token, status) => {
    // status = 0 (super admin), 1 (master_admin), 2 (admin), dan 3 (node)
    // var token = req.headers.authorization
    // console.log("AuthController", token, status)
    if (!token) {
        return { code: 401, status: false, message: "Unauthorized" }
    }

    const op = Sequelize.Op
    try {
        if (status === 0 || status === 1 || status === 2) {
            var hmac = config.hmac_admin
        } else if (status === 2) {
            var hmac = config.hmac_node
        }
        // console.log("authController", token, status)
        // console.log(hmac, hmac === config.hmac_sa)
        console.log(token)
        var data = await jwt.verify(token, hmac)
        console.log("data", data)
        // return {code: 202, status: true, message: "JWT Valid"}
    } catch (err) {
        // if (!err instanceof jwt.TokenExpiredError) {
        return { code: 401, status: false, message: err.message }
        // }
    }

    var username = data.username

    if (status === 0 || status === 1 || status === 2) {
        try {
            console.log("AuthController admin sync")
            await Admin.sync()
        } catch (e) {
            // console.log(e)
            return { code: 500, status: false, message: "Internal Server Error" }
        }

        var adminGrade = [0]
        if (status === 1) {
            adminGrade = [0, 1]
        } else if (status === 2) {
            adminGrade = [0, 1, 2]
        }

        var whereAdmin = { [op.and]: [{ admin_username: username }, { admin_grade: adminGrade }] }
        
        // if (status === 1) {
        //     whereAdmin = { [op.and]: [{ admin_username: username }, { admin_grade: 1 }] }
        // } else if (status === 2) {
        //     whereAdmin = { admin_username: username }
        // }

        // var count = await Admin.count({ where: whereAdmin })
        var adminData = await Admin.findOne({ where: whereAdmin })

        console.log('authController adminData', adminData, whereAdmin)

        if (count === 0) {
            return { code: 401, status: false, message: "Unauthorized" }
        } else {
            return { code: 202, status: true, message: "JWT Valid", adminData: adminData }
        }
    } else if (status === 3) {
        try {
            await Node.sync()
        } catch (e) {
            console.log(e)
            return { code: 500, status: false, message: "Internal Server Error" }
        }

        var count = await Node.count({ where: { node_username: username } })

        if (count === 0) {
            return { code: 401, status: false, message: "Unauthorized" }
        } else {
            return { code: 202, status: true, message: "JWT Valid", username: username }
        }
    }

    return { code: 401, status: false, message: "Unauthorized" }
}

module.exports = authController