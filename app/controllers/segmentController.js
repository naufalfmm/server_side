'use strict'

var Sequelize = require('sequelize'),
    db = require('../services/database');
var Segment = require('../models/segment'),
    Path = require('../models/path'),
    Lane = require('../models/lane');
var PathController = require('./pathController'),
    AuthController = require('./authController');

var segmentController = {}

segmentController.addSegmentOff = async (data, transaction) => {
    console.log("addSegmentOff", data)

    if (data.start === undefined || data.end === undefined ||
        data.paths === undefined || data.density === undefined || data.firstPath === undefined) {
        return ({ code: 400, status: false, message: "Data Incomplete" })
    }

    for (let key in data) {
        if (data[key] === '' && (key !== 'firstPath' || key !== 'secondPath'))
            return ({ code: 400, status: false, message: "Data Incomplete" })
    }

    if (data.paths === 2) {
        if (data.secondPath === undefined) {
            return ({ code: 400, status: false, message: "Data Incomplete" })
        }
    }

    var segmentData = null
    try {
        var newSegment = {
            segment_start: data.start,
            segment_end: data.end,
            segment_paths: data.paths,
            segment_density: data.density,
            segment_road_id: data.roadId
        }

        if (transaction !== null) segmentData = await Segment.create(newSegment, {transaction: transaction})
        else segmentData = await Segment.create(newSegment)
    } catch (e) {
        console.log(e)
        return { code: 500, status: false, message: "Internal Server Error" }
    }

    var segmentId = segmentData.dataValues.segment_id

    var pathsData = [{ ...data.firstPath, segmentId: segmentId }]
    if (data.paths === 2) {
        pathsData.push({ ...data.secondPath, segmentId: segmentId })
    }

    var statRet = await PathController.addPathsOff(pathsData, transaction)
    if (!statRet.status) {
        return { code: statRet.code, status: statRet.status, message: statRet.message }
    }

    return { status: true }
}

/*
link: api/segment/add/:roadId
json: {
    start:,
    end:,
    paths:,
    density:,
    firstPath:,
    secondPath: (optional)
}
*/
segmentController.addSegment = async (req, res) => {
    var auth = req.headers.auth
    var ret = await AuthController.checkAuthentication(auth, 1)

    if (!ret.status) {
        return res.status(ret.code).json({ status: ret.status, message: ret.message })
    }

    console.log("addSegment", req.body)

    if (req.body.start === undefined || req.body.end === undefined ||
        req.body.paths === undefined || req.body.density === undefined || 
        req.body.firstPath === undefined) {
        return res.status(400).json({ status: false, message: "Data Incomplete" })
    }

    var pathsData = []
    for (let key in req.body) {
        if (req.body[key] === '' && (key !== 'firstPath' || key !== 'secondPath'))
            return res.status(400).json({ status: false, message: "Data Incomplete" })
    }

    if (req.body.paths === 2) {
        if (req.body.secondPath === undefined) {
            return res.status(400).json({ status: false, message: "Data Incomplete" })
        }
    }

    var transSegment = null
    try {
        transSegment = await db.transaction()
    } catch (e) {
        await transSegment.rollback()
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    var segmentData = null
    try {
        var newSegment = {
            segment_start: req.body.start,
            segment_end: req.body.end,
            segment_paths: req.body.paths,
            segment_density: req.body.density,
            segment_road_id: req.params.roadId
        }

        segmentData = await Segment.create(newSegment, {transaction: transSegment})
    } catch (e) {
        await transSegment.rollback()
        if (!(e instanceof Sequelize.DatabaseError)) return res.status(403).json({ status: false, message: e.message })
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    var segmentId = segmentData.dataValues.segment_id
    var pathsData = [{ ...req.body.firstPath, segmentId: segmentId }]
    console.log(req.body.paths === 2)
    if (req.body.paths === 2) {
        pathsData.push({ ...req.body.secondPath, segmentId: segmentId })
    }

    var statRet = await PathController.addPathsOff(pathsData, transSegment)
    if (!statRet.status) {
        await transSegment.rollback()
        return res.status(statRet.code).json({ status: statRet.status, message: statRet.message })
    }

    if (transSegment !== null) await transSegment.commit()
    return res.status(201).json({ status: true, message: "Segment Successfully Added" })
}

/*
link: api/segment/delete/:segmentId
*/
segmentController.deleteSegment = async (req, res) => {
    var auth = req.headers.auth
    var ret = await AuthController.checkAuthentication(auth, 1)

    if (!ret.status) {
        return res.status(ret.code).json({ status: ret.status, message: ret.message })
    }
    console.log("deleteSegment")

    if (req.params.segmentId === null) {
        return res.status(400).json({ status: false, message: "Data Incomplete" })
    }

    var segmentId = req.params.segmentId

    try {
        await Segment.sync()
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    try {
        var rowDeleted = await Segment.destroy({ where: { 'segment_id': segmentId } })
        if (rowDeleted === 0) {
            return res.status(403).json({ status: false, message: "Segment Doesn't Exist" })
        }
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }

    return res.status(200).json({ status: true, message: "Segment is Successfully Deleted" })
}

/*
link: api/segment/edit/:segmentId
json: {
    start:,
    end:,
    paths:,
    density:,
    firstPath:,
    secondPath: (optional)
}
*/
segmentController.editSegment = async (req, res) => {
    var auth = req.headers.auth
    var ret = await AuthController.checkAuthentication(auth, 1)

    if (!ret.status) {
        return res.status(ret.code).json({ status: ret.status, message: ret.message })
    }
    console.log("editSegment")

    if (req.body.start === undefined || req.body.end === undefined || 
        req.body.paths === undefined || req.body.density === undefined || req.body.firstPath === undefined) {
        return res.status(400).json({ status: false, message: "Data Incomplete" })
    }
    console.log(0)
    for (let key in req.body) {
        if (req.body[key] === '' && (key !== 'firstPath' || key !== 'secondPath'))
            return res.status(400).json({ status: false, message: "Data Incomplete" })
        else if (req.body[key].length === 0)
            return res.status(400).json({ status: false, message: "Data Incomplete" })
    }
    console.log(1)
    var segmentId = req.params.segmentId
    if (segmentId === null) {
        return res.status(400).json({ status: false, message: "Data Incomplete" })
    }
    console.log(2)
    try {
        await Segment.sync()
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }
    console.log(3)
    var segmentData = null
    try {
        segmentData =  await Segment.findOne({ where: { segment_id: segmentId }, include: [{ model: Path, include: [{ model: Lane }] }] })
    } catch (e) {
        return res.status(500).json({ status: false, message: "Internal Server Error" })
    }
    // console.log(segmentData)
    // var pathsReq = req.body.pathsData.slice()
    // console.log(segmentData.paths)
    var op = Sequelize.Op
    var transSegment = null
    console.log(4)
    if (segmentData.segment_paths > req.body.paths) { //Path changes 2 to 1
        console.log("if")
        var pathData = segmentData.paths.slice()

        var laneUsed = [[],[]]
        var pathUsed = [{},{}]
        for (var i=0; i<pathData.length; i++) {
            var lanePaths = pathData[i].lanes.slice()
            var lanePath = lanePaths.map(lane => lane.lane_th)
            lanePath.sort()
            laneUsed[pathData[i].path_th] = lanePath.slice()
            pathUsed[pathData[i].path_th] = {...pathData[i]}
        }

        const laneChange = laneUsed[1].slice()
        const pathChange = { ...pathUsed[1] }
        const laneKeep = laneUsed[0].slice()
        const pathKeep = { ...pathUsed[0] }

        var laneNumber = (pathKeep.dataValues.path_lanes === req.body.firstPath.lanes ? pathKeep.dataValues.path_lanes : req.body.firstPath.lanes)
        var laneCount = laneKeep.length
        var laneRest = laneNumber - laneCount

        console.log("laneRest", laneRest)
        if (laneRest > 0) {
            try {
                transSegment = await db.transaction()
            } catch (e) {
                await transSegment.rollback()
                return res.status(500).json({ status: false, message: "Internal Server Error" })
            }
            var j = 0
            for (var i = 0; i < laneChange.length; i++) {
                console.log(laneChange[i], pathKeep.dataValues.path_lanes, laneNumber)
                if (laneChange[i] + pathKeep.dataValues.path_lanes < laneNumber) {
                    await Lane.update({ lane_th: laneChange[i] + pathKeep.dataValues.path_lanes, lane_path_id: pathKeep.dataValues.path_id }, 
                        {
                            where: { [op.and]: [{ lane_th: laneChange[i] }, { lane_path_id: pathChange.dataValues.path_id } ] }, 
                            transaction: transSegment })
                } else {
                    break
                }
            }
        }

        // var idxData = 0,
        //     idxChange = 1
        // if (pathData[idxData].dataValues.path_th === 1) idxData = 1
        // if (idxData === 1) idxChange = 0

        // // var idxReq = 0
        // // if (pathsReq[idxReq].th === 1) idxReq = 1

        // var laneNumber = (pathData[idxData].dataValues.path_lanes === req.body.firstPath.lanes ? pathData[idxData].dataValues.path_lanes : req.body.firstPath.lanes)
        // var laneCount = pathData[idxData].dataValues.lanes.length
        // var laneRest = laneNumber - laneCount
        // console.log("laneNumber, laneCount, laneRest", laneNumber, laneCount, laneRest)

        // if (laneRest > 0) {
        //     try {
        //         transSegment = await db.transaction()
        //     } catch (e) {
        //         await transSegment.rollback()
        //         return res.status(500).json({ status: false, message: "Internal Server Error" })
        //     }
        //     console.log("slice error", pathData[idxChange], pathData)
        //     var laneData = pathData[idxChange].dataValues.lanes.slice()
        //     for (var i=0; i<laneData.length; i++) {
        //         try {
        //             console.log(i, laneRest, i<laneRest)
        //             if (i < laneRest) 
        //                 await Lane.update({ lane_th: laneCount + i, lane_path_id: pathData[idxData].dataValues.path_id }, 
        //                     { where: { [op.and]: [{ lane_th: i }, { lane_path_id: pathData[idxChange].dataValues.path_id } ] }, 
        //                     transaction: transSegment })
        //             else
        //                 await Lane.destroy({where: { [op.and]: [{ lane_th: i }, 
        //                     { lane_path_id: pathData[idxChange].dataValues.path_id }] }, transaction: transSegment })
        //         } catch (e) {
        //             await transSegment.rollback()
        //             return res.status(500).json({ status: false, message: "Internal Server Error" })
        //         }
        //     }
        //     // for (var i=0; i<laneCount; i++) {
        //     //     console.log(pathData[idxData].dataValues.path_id, laneData[idxChange].lane_id, laneRest, i)
        //     //     try {
        //     //         if (i < laneRest) await Lane.update({ lane_th: laneCount + i, lane_path_id: pathData[idxData].dataValues.path_id }, { where: { lane_id: laneData[i].lane_id }, transaction: transSegment })
        //     //         else await Lane.destroy({ where: { lane_id: laneData[i].lane_id }})
        //     //     } catch (e) {
        //     //         await transSegment.rollback()
        //     //         return res.status(500).json({ status: false, message: "Internal Server Error" })
        //     //     }
        //     // }
        // }

        var statRet = await PathController.editPathsOff([{ ...req.body.firstPath }], transSegment)
        if (!statRet.status) {
            if (transSegment !== null) await transSegment.rollback()
            return res.status(statRet.code).json({ status: statRet.status, message: statRet.message })
        }

        console.log(pathChange.dataValues.path_id)

        statRet = await PathController.deletePathsOff([pathChange.dataValues.path_id], transSegment)
        if (!statRet.status) {
            if (transSegment !== null) await transSegment.rollback()
            return res.status(statRet.code).json({ status: statRet.status, message: statRet.message })
        }
    } else if (segmentData.segment_paths < req.body.paths) { //Path changes 1 to 2
        console.log("else if")
        if (req.body.secondPath === undefined) {
            return res.status(400).json({ status: false, message: "Data Incomplete" })
        }

        var pathData = segmentData.paths.slice()

        // Add new path
        try {
            transSegment = await db.transaction()
        } catch (e) {
            if (transSegment !== null) await transSegment.rollback()
            return res.status(500).json({ status: false, message: "Internal Server Error" })
        }

        //Add second path
        var pathAdd = [{ ...req.body.secondPath, segmentId: segmentId }]
        var pathRet = await PathController.addPathsOff(pathAdd, transSegment)
        if (!pathRet.status) {
            if (transSegment !== null) await transSegment.rollback()
            return res.status(statRet.code).json({ status: statRet.status, message: statRet.message })
        }

        var newLane = req.body.firstPath.lanes
        var newSecond = req.body.secondPath.lanes
        console.log(newLane, newSecond, pathData[0], pathData[0].dataValues.path_lanes)
        for (var i = newLane; i < pathData[0].dataValues.path_lanes; i++) {
            console.log("for", i, newLane, newSecond, pathData[0].dataValues.path_lanes)
            if (i-newLane < newSecond) {
                await Lane.update({ lane_th: i - newLane, lane_path_id: pathRet.pathsId },
                    {
                        where: { [op.and]: [{ lane_th: i }, { lane_path_id: pathData[0].dataValues.path_id }] },
                        transaction: transSegment
                    })
            }
        }  

        // var idxData = 0,
        //     idxChange = 1
        // if (pathData[idxData].path_th === 1) idxData = 1
        // if (idxData === 1) idxChange = 0

        // // var idxReq = 0,
        // //     idxReqEdit = 1
        // // if (pathsReq[idxReq].th === 1) idxReq = 1
        // // if (idxReq === 1) idxReqEdit = 0

        // // Add new path
        // try {
        //     transSegment = await db.transaction()
        // } catch (e) {
        //     await transSegment.rollback()
        //     return res.status(500).json({ status: false, message: "Internal Server Error" })
        // }

        // //Add second path
        // var pathAdd = [{ ...req.body.secondPath, segmentId: segmentId }]
        // var pathRet = await PathController.addPathsOff(pathAdd, transSegment)
        // if (!pathRet.status) {
        //     await transSegment.rollback()
        //     return res.status(statRet.code).json({ status: statRet.status, message: statRet.message })
        // }

        // if (pathData[idxData].path_lanes > req.body.firstPath.lanes) {
        //     var laneCount = pathData[idxData].lanes.length
        //     var laneNumber = req.body.firstPath.lanes
        //     var laneRest = laneCount - laneNumber
        //     var laneData = pathData[idxData].lanes.slice()
        //     var laneAccept = req.body.secondPath.lanes

        //     var pathId = pathRet.pathsId

        //     var j = 0
        //     for (var i=laneNumber; i<laneCount; i++) {
        //         try {
        //             if (j < laneAccept) 
        //                 await Lane.update({ lane_th: i - laneNumber, lane_path_id: pathId }, 
        //                     { where: { [op.and]: [{ lane_th: i }, { lane_path_id: pathData[idxData].path_id }] }, 
        //                     transaction: transSegment })
        //             else break
        //             j++
        //         } catch (e) {
        //             await transSegment.rollback()
        //             return res.status(500).json({ status: false, message: "Internal Server Error" })
        //         }
        //     }
        // }

        var statRet = await PathController.editPathsOff([{...req.body.firstPath}], transSegment)
        if (!statRet.status) {
            if (transSegment !== null) await transSegment.rollback()
            return res.status(statRet.code).json({ status: statRet.status, message: statRet.message })
        }
    } else {
        console.log("else")
        try {
            transSegment = await db.transaction()
        } catch (e) {
            await transSegment.rollback()
            return res.status(500).json({ status: false, message: "Internal Server Error" })
        }

        var data = [{...req.body.firstPath}]
        if (req.body.paths > 1) {
            data.push({...req.body.secondPath})
        }
        var statRet = await PathController.editPathsOff(data, transSegment)
        if (!statRet.status) {
            if (transSegment !== null) await transSegment.rollback()
            return res.status(statRet.code).json({ status: statRet.status, message: statRet.message })
        }

        // for (var i=0; i<req.body.pathsData.length; i++) {
        //     var statRet = await PathController.editPathsOff(req.body.pathsData[i].slice(), transSegment)
        //     if (!statRet.status) {
        //         await transSegment.rollback()
        //         return res.status(statRet.code).json({ status: statRet.status, message: statRet.message })
        //     }
        // }
    }
    //
    try {
        var updateSegment = {
            segment_start: req.body.start,
            segment_end: req.body.end,
            segment_paths: req.body.paths,
            segment_density: req.body.density
        }

        if (transSegment !== null) await Segment.update(updateSegment, { where: { segment_id: segmentId }, transaction: transSegment })
        else await Segment.update(updateSegment, { where: { segment_id: segmentId } })
    } catch (e) {
        if (transSegment !== null) await transSegment.rollback()
        return res.status(500).json({ status: false, message: e.message })
    }
    
    if (transSegment !== null) await transSegment.commit()
    return res.status(200).json({ status: true, message: "Segment Successfully Edited" })
}

module.exports = segmentController;