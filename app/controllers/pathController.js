'use strict';

var Sequelize = require('sequelize'),
    db = require('../services/database');
var Path = require('../models/path'),
    Lane = require('../models/lane');

var pathController = {}

/*
Offline version (not able to used in API)
Parameter: 
- data: []
- transaction: sql transaction (optional)
*/
pathController.addPathsOff = async (data, transaction) => {
    console.log('addPathsOff', data, data.length)
    try {
        await Path.sync()
    } catch (e) {
        console.log(e)
        return { code: 500, status: false, message: "Internal Server Error" }
    }
    console.log(0)

    if (data.length === 2) {
        var checkData = data.slice()
        var thCheck = checkData.map(t => t.th)

        if (thCheck.indexOf(0) === -1 || thCheck.indexOf(1) === -1) {
            return { code: 400, status: false, message: "Data Incomplete" }
        }

        var sortData = [{},{}]
        for (var i=0; i<data.length; i++) {
            sortData[data[i].th] = {...data[i]}
        }

        data = sortData.slice()
    }

    // var pathData = null
    var pathsId = []
    for (var i=0; i<data.length; i++) {
        console.log(data[i], data[i].th, data[i].lanes, data[i].segmentId)
        if (data[i].th === undefined || data[i].lanes === undefined || data[i].segmentId === undefined) {
            return { code: 400, status: false, message: "Data Incomplete" }
        }

        for (let key in data[i]) {
            if (data[i][key] === '')
                return { code: 400, status: false, message: "Data Incomplete" }
        }
        console.log(data[i])
        try {
            var newPath = {
                path_th: data[i].th,
                path_lanes: data[i].lanes,
                path_segment_id: data[i].segmentId
            }

            if (transaction !== null) var pathData = await Path.create(newPath, { transaction: transaction })
            else var pathData = await Path.create(newPath, { transaction: transaction })

            console.log('after create', pathData)

            pathsId.push(pathData.path_id)
        } catch (e) {
            // console.log(e, e instanceof Sequelize.Error, e instanceof Sequelize.DatabaseError, e instanceof Error, e.message)
            if (!(e instanceof Sequelize.DatabaseError) || e instanceof Error) return { code: 403, status: false, message: e.message }
            return { code: 500, status: false, message: "Internal Server Error" }
        }
    }

    return { status: true, pathsId: pathsId.length > 1 ? pathsId : pathsId[0] }
}

/*
Offline version (not able to used in API)
Parameter:
- id: []
- transaction: sql transaction (optional)
*/
pathController.deletePathsOff = async (id, transaction) => {
    console.log("deletePathsOff")
    try {
        await Path.sync()
    } catch (e) {
        return { code: 500, status: false, message: "Internal Server Error" }
    }

    var trans = transaction
    if (trans === null) trans = await db.transaction()

    try {
        var rowDeleted = await Path.destroy({ where: { path_id: id }, transaction: trans })
        if (rowDeleted < id.length) {
            if (transaction === null) await trans.rollback()
            return { code: 403, status: false, message: "Path Doesn't Exists" }
        }
    } catch (e) {
        if (transaction === null) await trans.rollback()
        if (!(e instanceof Sequelize.DatabaseError)) return { code: 403, status: false, message: e.message }
        return { code: 500, status: false, message: "Internal Server Error" }
    }

    if (transaction === null) await trans.commit()
    return { status: true }
}

/*
Offline version (not able to used in API)
Parameter:
- data: []
- transaction: sql transaction (optional)
*/
pathController.editPathsOff = async (data, transaction) => {
    console.log(data)
    try {
        await Path.sync()
    } catch (e) {
        return { code: 500, status: false, message: "Internal Server Error" }
    }

    var op = Sequelize.Op

    if (data.length === 2) {
        var checkData = data.slice()
        var thCheck = checkData.map(t => t.th)

        if (thCheck.indexOf(0) === -1 || thCheck.indexOf(1) === -1) {
            return { code: 400, status: false, message: "Data Incomplete" }
        }
    } else {
        var pathsCount = 0
        try {
            var pathData = await Path.findOne({ where: { path_id: data[0].id } })
            pathsCount = await Path.count({ where: { [op.and]: [{ path_segment_id: pathData.path_segment_id }, { path_id: { [op.ne]: data[0].id } }, { path_th: { [op.ne]: data[0].th } }] } })
        } catch (e) {
            return { code: 500, status: false, message: "Internal Server Error" }
        }

        if (pathsCount > 0) {
            return { code: 403, status: false, message: "No Path Edited" }
        }
    }

    for (var i=0; i<data.length; i++) {
        console.log(data[i])
        if (data[i].id === undefined || data[i].th === undefined || data[i].lanes === undefined) {
            return { code: 400, status: false, message: "Data Incomplete" }
        }

        for (let key in data[i]) {
            if (data[i][key] === '')
                return { code: 400, status: false, message: "Data Incomplete" }
        }

        var pathId = data[i].id
        var pathLane = data[i].lanes
        var pathTh = data[i].th
        var pathData = null
        try {
            pathData = await Path.findOne({ where: { path_id: pathId }, include: [{ model: Lane }] })
            if (pathData === null) return { code: 403, status: false, message: "No Path Edited" }
        } catch (e) {
            return { code: 500, status: false, message: "Internal Server Error" }
        }
        // console.log(pathData)
        // pathData = pathData.dataValues
        var laneData = pathData.path_lanes

        console.log("editPathsOff", pathLane, laneData)

        var op = Sequelize.Op
        
        if (pathLane < laneData) {
            for (let i=laneData - 1; i>=pathLane; i--) {
                try {
                    if (transaction !== null) await Lane.destroy({ where: { [op.and]: [{ lane_th: i }, { lane_path_id: pathId }] }, transaction: transaction })
                    else await Lane.destroy({ where: { [op.and]: [{ lane_th: i }, { lane_path_id: pathId }] } })
                } catch (e) {
                    console.log(e)
                    return { code: 500, status: false, message: "Internal Server Error (Lane Destroy)" }
                }
            }
        }

        // if (pathLane < laneData.length) {
        //     //Delete some lanes till the number of lanes is equal to path_lanes
        //     var laneRest = laneData.length - pathLane
        //     console.log(laneRest, laneData.length, pathLane)
        //     for (var i=pathLane; i<laneData.length; i++) {
        //         try {
        //             if (transaction !== null) await Lane.destroy({ where: { [op.and]: [{ lane_th: i }, { lane_path_id: pathId }] }, transaction: transaction })
        //             else await Lane.destroy({ where: { [op.and]: [{ lane_th: i }, { lane_path_id: pathId }] } })
        //         } catch (e) {
        //             console.log(e)
        //             return { code: 500, status: false, message: "Internal Server Error (Lane Destroy)" }
        //         }
        //     }
        //     // for (var i = laneData.length - 1; i >= laneData.length - laneRest - 1; i++) {
        //     //     // console.log(laneData[i], laneData.length, i)
        //     //     try {
        //     //         if (transaction !== null) await Lane.destroy({ where: { lane_id: laneData[i].lane_id }, transaction: transaction })
        //     //         else await Lane.destroy({ where: { lane_id: laneData[i].lane_id } })
        //     //     } catch (e) {
        //     //         console.log(e)
        //     //         return { code: 500, status: false, message: "Internal Server Error" }
        //     //     }
        //     // }
        // }
        // console.log(data[i])
        try {
            // console.log(data[i])
            var updatePath = {
                path_th: pathTh,
                path_lanes: pathLane
            }

            if (transaction !== null) var pathEdit = await Path.update(updatePath, { where: { path_id: pathId }, transaction: transaction })
            else var pathEdit = await Path.update(updatePath, { where: { path_id: pathId } })

            if (pathEdit === 0) return { code: 403, status: false, message: "No Path Edited" }
        } catch (e) {
            console.log(e)
            if (!(e instanceof Sequelize.DatabaseError)) return { code: 403, status: false, message: e.message }
            return { code: 500, status: false, message: "Internal Server Error" }
        }
    }
    

    return { status: true, message: "Path Successfully Edited" }
}

module.exports = pathController