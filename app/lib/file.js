'use strict'

var fs = require('fs'),
    path = require('path');

var fileFolder = {}

fileFolder.createFolder = (url) => {
    var arrUrl = url.split(path.sep)
    var urlTrig = "."
    // console.log(arrUrl, url)
    try {
        for (var i = 1; i < arrUrl.length; i++) {
            // console.log(urlTrig,arrUrl[i])
            urlTrig += path.sep + arrUrl[i]
            if (!fs.existsSync(urlTrig)) {
                fs.mkdirSync(urlTrig)
            }
        }  
    } catch (e) {
        return {status: false, code: 500, message: "Internal Server Error"}
    }

    return {status: true, code: 201, message: "Path is Successfully Created"}
}

fileFolder.checkFile = (path) => {
    if (!fs.existsSync(path)) {
        return { status: false, code: 404, message: "File Didn't Exists" }
    }

    return { status: true, code: 200, message: "File Exists!" }
}

module.exports = fileFolder