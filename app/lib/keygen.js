'use strict'

var keygen = {}

keygen.randomKey = (len) => {
    var char = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789`~!@#$%^&*()-_=+[{]}\|;:'<,>.?/"

    var key = ""
    for (var i=0; i<len; i++) {
        var posChar = Math.floor(Math.random() * char.length)
        key += char[posChar]
    }

    return key
}

module.exports = keygen