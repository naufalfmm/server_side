'use strict';

var keygen = require('./keygen'),
    crypto = require('crypto');
var Node = require('../models/node')

var removePadding = (textpad) => {
    var pad = textpad[textpad.length - 1],
        re = new RegExp(pad, "g");

    return textpad.replace(re, '')
}

var addPadding = (message) => {
    var charCode = 16 - (message.length % 16)
    var pad = String.fromCharCode(charCode)
    for (var i=0; i<charCode; i++) {
        message += pad
    }

    return message
}

var checkSHA512 = (message, check) => {
    console.log(typeof check)
    var shasum = crypto.createHash('sha512')
    shasum.update(message,'utf8')
    var hash_check = shasum.digest('hex')
    if (check != hash_check) {
        return false
    }
    return true
}

var encryptAES = {}

encryptAES.generatekey = async () => {
    var key = keygen.randomKey(32)
    var check = await Node.count({where: {node_key: key}})

    while (check > 0) {
        key = keygen.randomKey(32)
        check = await Node.count({ where: { node_key: key } })
    }

    return key
}

encryptAES.decryption = (cipher, key) => {
    var decipher = crypto.createDecipheriv('aes-256-ecb',key, "")
    decipher.setAutoPadding(false)
    var text = decipher.update(cipher, 'base64', 'utf8')
    text += decipher.final('utf8')

    return removePadding(text)
}

encryptAES.encryption = (message, key) => {
    var cipher = crypto.createCipheriv('aes-256-ecb', key, "")
    cipher.setAutoPadding(false)
    message = addPadding(message)
    var crypted = cipher.update(message, 'utf8', 'base64')
    crypted += cipher.final('base64')

    return crypted
}

encryptAES.checkJSON = (message, check) => {
    console.log(typeof message, message)
    try {
        if (!checkSHA512(message, check)) {
            return { status: false, code: 401, message: "Unauthorized" }
        }
        console.log(message)
        var json_mess = JSON.parse(message)
        return {status: true, code: 200, message: json_mess}
    } catch (e) {
        return {status: false, code: 500, message: "Internal Server Error"}
    }
}

module.exports = encryptAES