'use strict';

var router = require('express').Router();
var AuthController = require('../controllers/authController'),
    RoadController = require('../controllers/roadController'),
    NodeController = require('../controllers/nodeController'),
    AdminController = require('../controllers/adminController'),
    LaneController = require('../controllers/laneController'),
    DataController = require('../controllers/dataController'),
    SegmentController = require('../controllers/segmentController');

var APIRoutes = function() {
    // console.log("APIRoutes")
    //Node
    router.post('/node/login', NodeController.login)
    router.post('/node/add', NodeController.addNode)
    router.post('/node/create/:nodeId', NodeController.createConfig)
    router.delete('/node/delete/:nodeId',NodeController.deleteNode)
    router.get('/node/download/:nodeId', NodeController.downloadConfig)
    router.get('/node/all', NodeController.getNodeAll)
    router.get('/node/:nodeId', NodeController.getNode)
    //Road
    router.post('/road/add', RoadController.addRoad)
    router.get('/road/all', RoadController.getRoadAll)
    router.delete('/road/delete/:roadId', RoadController.deleteRoad)
    router.get('/road/:roadId', RoadController.getRoad)
    router.post('/road/edit/:roadId', RoadController.editRoad)
    //Segment
    router.post('/segment/add/:roadId', SegmentController.addSegment)
    router.post('/segment/edit/:segmentId', SegmentController.editSegment)
    router.delete('/segment/delete/:segmentId', SegmentController.deleteSegment)
    //Admin
    router.post('/admin/login', AdminController.login)
    router.post('/admin/registration', AdminController.registration)
    router.delete('/admin/unregist/:adminId', AdminController.unregistration)
    router.delete('/admin/delete/:adminId', AdminController.deleteAdmin)
    router.post('/admin/status/:activeStatus/:adminId', AdminController.changeActiveStatus)
    router.get('/admin/all', AdminController.getAllAdmin)
    //Data
    router.post('/data/post', DataController.postData)
    router.get('/data/all', DataController.getDataAll)
    router.get('/data/severity', DataController.getSeverityData)
    router.get('/data/:dataId(\\d+)/', DataController.getDataId)
    router.get('/data/:sevStatus', DataController.getData)

    return router
}

module.exports = APIRoutes;