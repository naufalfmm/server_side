'use strict';

var express = require('express'),
    bodyParser = require('body-parser'),
    cors = require('cors');
    // http = require('http');
var db = require('./app/services/database');
var bcrypt = require('bcrypt');


//Initializations
var app = express();

//Handle CORS
app.use(cors());

//Parse as urlencoded and json
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//Bundle API routes
app.use('/api', require('./app/routes/api')());

//Start the server
// app.set('port',port)
var env = process.env.NODE_ENV

if (env !== 'test') {
    console.log('Not Test')
    var port = process.env.PORT || 7777;

    app.listen(port, async function () {
        try {
            await db.authenticate()
        } catch (e) {
            console.log('Internal Server Error Authenticate')
            throw new Error();

            // this.close()
            // return
        }
        try {
            await db.sync()
        } catch (e) {
            console.log('Internal Server Error Sync')
            // this.close()
            // return
            throw new Error()
        }
        console.log('Listening on ' + port)
    })

} else {
    db.authenticate().then(() => {
        db.sync()
    }).catch(e => {
        console.log('Internal Server Error')
        throw new Error()
    })
}

module.exports = app

// var server = http.createServer(app)
// server.listen(port);